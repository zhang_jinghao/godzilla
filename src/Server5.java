import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.StringTokenizer;


public class Server5 {

    private static Complex cplx1;
    private static Complex cplx2;
    private static String a,b;
    private static char ch;
    private static Complex result = null;

    public static void main(String[] args) throws IOException {
        //建立一个服务器ServerSocket绑定指定端口
        ServerSocket serverSocket=new ServerSocket(8800);
        //使用accept()方法阻止等待监听，获得新连接
        Socket socket=serverSocket.accept();
        //输入流
        InputStream inputStream=socket.getInputStream();
        BufferedReader bufferedReader=new BufferedReader(new InputStreamReader(inputStream));
        //输出流
        OutputStream outputStream=socket.getOutputStream();
        PrintWriter printWriter=new PrintWriter(outputStream);
        //读取用户输入信息
        String info=null;
        System.out.println("服务器已经建立......");
        while(!((info = bufferedReader.readLine()) ==null)) {
            System.out.println("我是服务器，用户信息为：" + info);


            StringTokenizer st = new StringTokenizer(info, " ", false);
            a = st.nextToken();
            ch = st.nextToken().charAt(0);
            b = st.nextToken();
            cplx1 = new Complex(a);
            cplx2 = new Complex(b);

            switch (ch) {
                case '+':
                    result = cplx1.ComplexAdd(cplx2);

                    break;
                case '-':
                    result = cplx1.ComplexSub(cplx2);

                    break;
                case '*':
                    result = cplx1.ComplexMulti(cplx2);

                    break;
                case '/':
                    result = cplx1.ComplexDiv(cplx2);
                    break;
                default:
                    break;
            }
        }

        //给客户一个响应
        String reply=cplx1+String.valueOf(ch)+cplx2+"="+result;
        printWriter.write(reply);
        printWriter.flush();
        //关闭资源
        printWriter.close();
        outputStream.close();
        bufferedReader.close();
        inputStream.close();
        socket.close();
        serverSocket.close();
    }
}
