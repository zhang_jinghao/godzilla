/*************************************************************************
    > 文件名: Book.java
    > 作者: 20182328张景昊
    > 时间: 2019年09月20日 
************************************************************************/



public class Book{
       private String name;

       private String writer;

       private String publisher;

       private String time;

public Book(String name,String writer,String publisher,String time)
    {
           this.setname(name);  

           this.setwriter(writer); 
 
           this.setpublisher(publisher);  

           this.settime(time);  
       }

       public void setname(String name)
{

           this.name = name;

       }

       public String getname()
{

           return name;

       }

       public void setwriter(String writer)
{

          this.writer = writer;

       }

       public String getwriter()
{

          return writer;

       }

       public void setpublisher(String publisher)
{

          this.publisher = publisher;

       }

       public String getpublisher()
{

          return publisher;
       }


       public void settime(String time)
{

          this.time = time;
       }


       public String gettime()
{

          return time;

       }

       public String toString()
{

         return "书名:"+this.getname()+"\n"+"作者:"+this.getwriter()+"\n"+"出版社:"+this.getpublisher()+"\n"+"时间:"+this.gettime();
       }

}
