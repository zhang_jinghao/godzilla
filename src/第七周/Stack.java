public interface Stack<T> {
    public void push(T e);		//进栈元素e
    public T pop();			//出栈元素e
    public T peek();		//查看当前栈顶元素，不取出
    public boolean isEmpty();	//判断栈是否为空
    public int size();
}