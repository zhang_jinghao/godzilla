import java.util.Scanner;
public class myLink {
    public static void main(String[] args) {

        Scanner Input = new Scanner(System.in);
        int num = 0;
        NumNode head = new NumNode(num);

        for (;;){
            System.out.println("请输入一个整数（以负数作为结尾）");
            int num1 = Input.nextInt();
            if(num1<0)
                break;
            NumNode x1 = new NumNode(num1);
            add1(head, x1);
        }
        NumNode x1 = new NumNode(114);
        NumNode x2 = new NumNode(514);
        NumNode x3 = new NumNode(222);
        NumNode x4 = new NumNode(333);


        System.out.println("添加第一个结点“114”");
        add1(head,x1);
        System.out.println("添加第二个结点“514”");
        add1(head,x2);
        System.out.println("添加第三个结点“222”");
        add1(head,x3);
        System.out.println("添加第四个结点“333”");
        add1(head,x4);

        Printlist(head);
        System.out.println();

        System.out.println("删除结点“114”");
        System.out.println();
        delete(head, x1);
        //删除结点
        Printlist(head);

        Sort(head);
        System.out.println();
        System.out.println("排序结果：");
        //得出排序结果
        Printlist(head);


    }
    private static class NumNode
    {
        protected NumNode next = null;
        protected int num;
        public NumNode(int num)
        {
            this.num = num;
            next = null;
        }
    }
    public static void Printlist(NumNode Head) {
        NumNode node = Head;
        while (node != null) {
            System.out.print(" " + node.num);
            node = node.next;
        }
    }

    public static void add1(NumNode head, NumNode node)
    {
        if ( head == null)
            head = node;
        else {
            NumNode temp = head;
            while (temp.next != null) {
                temp = temp.next;
            }
            temp.next = node;
        }
    }

    public static NumNode add2(NumNode head, NumNode node)
    {
        node.next = head;
        head = node;
        return head;
    }

    public static void delete(NumNode head, NumNode node) {
        NumNode b = head, c = head;

        while (b != null) {
            if (b.num != node.num) {
                c = b;
                b = b.next;
            } else {
                break;
            }
        }
    }

    public static void  Sort(NumNode head)
    {
        NumNode d = head;
        int temp;

        while (d != null)
        {
            NumNode numNode = d.next;
            while (numNode != null)
            {
                if (numNode.num < d.num)
                {
                    temp = d.num;
                    d.num = numNode.num;
                    numNode.num = temp;
                }
                numNode = numNode.next;
            }
            d = d.next;
        }
    }
}