public class SortTest1 {
    public static void main(String[] args) {
        Comparable[]array1={11,38,1,33,28,23,16};
        Comparable[]array2={26,38,7,45,28,23,84};
        Comparable[]array3={35,47,9,66,12,69,17};

        System.out.println("原数组为：");
        for (Comparable num1:array1)
            System.out.print(num1+" ");
        System.out.println("选择排序：");
        System.out.println(Sorting.selectionSort(array1));

        System.out.println("原数组为：");
        for (Comparable num2:array2)
            System.out.print(num2+" ");
        System.out.println("选择排序：");
        System.out.println(Sorting.selectionSort(array2));

        System.out.println("原数组为：");
        for (Comparable num3:array3)
            System.out.print(num3+" ");
        System.out.println("选择排序：");
        System.out.println(Sorting.selectionSort(array3));
    }
}