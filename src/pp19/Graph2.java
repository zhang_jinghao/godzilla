import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

public class Graph2 {
    private List<Vertex> vertices;
    private int edgeCount;
    private List<Vertex> depth;
    private List<Vertex> breadth;

    protected void breadthFirstVisit(char beginLabel){

        Vertex origin=this.getVertexByChar(beginLabel);
        if(origin==null)return;
        List<Vertex> queue=new LinkedList<Vertex>();
        origin.setVisited(true);
        queue.add(origin);
        breadth.add(origin);
        Vertex curVertex=null;
        while(!queue.isEmpty()){
            curVertex=queue.remove(0);
            while(curVertex.getFirstUnvisitedNeighbor()!=null){
                Vertex tmpVertex=curVertex.getFirstUnvisitedNeighbor();
                tmpVertex.setVisited(true);
                queue.add(tmpVertex);
                breadth.add(tmpVertex);
            }

        }
        printVertexList(breadth);
    }

    protected void depthFirstVisit(char beginLabel){

        Vertex origin=this.getVertexByChar(beginLabel);
        if(origin==null)return;
        Stack<Vertex> stack=new Stack<Vertex>();
        origin.setVisited(true);
        stack.push(origin);
        depth.add(origin);
        Vertex curVertex=null;
        while(!stack.isEmpty()){
            curVertex=stack.peek();
            Vertex tmpVertex=curVertex.getFirstUnvisitedNeighbor();
            if(tmpVertex!=null){
                tmpVertex.setVisited(true);
                depth.add(tmpVertex);
                stack.push(tmpVertex);
            }else{
                stack.pop();
            }
        }
        printVertexList(depth);
    }

    public double getShortestPath(char beginLabel,char endLabel,Stack<Vertex> stack){
        resetVertices();
        Vertex begin=this.getVertexByChar(beginLabel);
        Vertex end=this.getVertexByChar(endLabel);
        begin.setVisited(true);
        List<Vertex> queue=new LinkedList<Vertex>();
        queue.add(begin);
        boolean done=false;
        while(!done&&!queue.isEmpty()){
            Vertex curVertex=queue.remove(0);
            while(!done&&curVertex.getFirstUnvisitedNeighbor()!=null){
                Vertex tmpVertex=curVertex.getFirstUnvisitedNeighbor();
                tmpVertex.setVisited(true);
                double  tmpCost=curVertex.getCost()+1;
                tmpVertex.setPreviousVertex(curVertex);
                tmpVertex.setCost(tmpCost);
                queue.add(tmpVertex);
                if(tmpVertex.equals(end)){
                    done=true;
                }
            }
        }
        double pathLength=end.getCost();
        while(end!=null){
            stack.push(end);
            end=end.getPreviousVertex();
        }
        return pathLength;
    }

    private boolean addEdge(char beginLabel, char endLabel, double weight){
        int beginIndex=vertices.indexOf(new Vertex(beginLabel));
        int endIndex=vertices.indexOf(new Vertex(endLabel));
        Vertex beginVertex=vertices.get(beginIndex);
        Vertex endVertex=vertices.get(endIndex);
        boolean result=beginVertex.connect(endVertex,weight);
        edgeCount++;
        return result;
    }
    protected boolean addEdge(char beginLabel, char endLabel){
        return addEdge(beginLabel,endLabel,0);
    }
    /**
     * @param label
     */
    protected void addVertex(char label){
        boolean result=false;
        Vertex newVertex=new Vertex(label);
        if(!vertices.contains(newVertex)){
            vertices.add(newVertex);
            result=true;
        }
    }
    public void printVertexList(List<Vertex> list){
        int i=0,len=list.size();
        while (i<len) {
            Vertex vertex=list.get(i);
            System.out.print(vertex.getLabel()+" ");
            i++;
        }
        System.out.println();
    }

    public void resetVertices(){
        int i=0,len=vertices.size();
        while (i<len) {
            Vertex vertex=vertices.get(i);
            vertex.setPreviousVertex(null);
            vertex.setVisited(false);
            vertex.setCost(0);
            i++;
        }
    }

    public Vertex getVertexByChar(char target){
        Vertex vertex=null;
        int i=0,len=vertices.size();
        while (i<len) {
            vertex=vertices.get(i);
            Character xx=vertex.getLabel();
            if(!(xx.charValue() != target)){
                return vertex;
            }
            i++;
        }
        return vertex;
    }

    protected Graph2(){
        vertices=new ArrayList<Vertex>();
        edgeCount=0;
        depth=new ArrayList<Vertex>();
        breadth=new ArrayList<Vertex>();
    }

    public static Graph2 createGapth(){


        Graph2 graph=new Graph2();
        graph.addVertex('0');
        graph.addVertex('1');
        graph.addVertex('2');
        graph.addVertex('3');
        graph.addVertex('4');
        graph.addVertex('5');
        graph.addVertex('6');
        graph.addVertex('7');

        graph.addEdge('0','4');
        graph.addEdge('0','3');
        graph.addEdge('0','1');

        graph.addEdge('1','4');
        graph.addEdge('1','2');
        graph.addEdge('1','0');

        graph.addEdge('2','5');
        graph.addEdge('2','1');

        graph.addEdge('3','6');
        graph.addEdge('3','0');

        graph.addEdge('4','6');
        graph.addEdge('4','1');
        graph.addEdge('4','0');

        graph.addEdge('5','2');

        graph.addEdge('6','7');
        graph.addEdge('6','4');
        graph.addEdge('6','3');

        graph.addEdge('7','6');

        return graph;
    }
}


class Vertex {
    private char label;
    private List<Edge> edgeList;
    private boolean isVisited;
    private Vertex previousVertex;
    private double cost;

    Vertex(char label) {
        this.label = label;
        edgeList = new ArrayList<Edge>();
        isVisited = false;
        previousVertex = null;
        cost = 0;
    }

    public boolean isVisited() {
        return isVisited;
    }

    public void visit() {
        System.out.println(this.label);
        this.isVisited = true;
    }

    void setPreviousVertex(Vertex vertex) {
        this.previousVertex = vertex;
    }

    void setVisited(Boolean isVisited) {
        this.isVisited = isVisited;
    }

    void setCost(double cost) {
        this.cost = cost;
    }

    public Vertex getFirstNeighbor() {
        return this.edgeList.get(0).endVertex;
    }

    char getLabel() {
        return this.label;
    }

    double getCost() {
        return this.cost;
    }

    public Vertex getPreviousVertex() {
        return this.previousVertex;
    }

    Vertex getFirstUnvisitedNeighbor() {
        Vertex unVisitedNeighbor = null;
        int i = 0, len = edgeList.size();
        while (i < len) {
            Vertex vertex = edgeList.get(i).endVertex;
            if (!vertex.isVisited) {
                unVisitedNeighbor = vertex;
                break;
            }
            i++;
        }
        return unVisitedNeighbor;
    }

    public boolean equals(Object object) {
        boolean result = false;
        if (this == object) return true;
        if (object instanceof Vertex) {
            Vertex otherVertex = (Vertex) object;
            result = this.label == otherVertex.label;
        }
        return result;
    }

    public boolean connect(Vertex endVertex, double weight) {

        boolean result = false;

        if (!this.equals(endVertex)) {
            boolean isDuplicateEdge = false;
            List<Edge> edgeList = this.edgeList;
            if (edgeList.contains(endVertex)) isDuplicateEdge = true;
            if (!isDuplicateEdge) {
                edgeList.add(new Edge(endVertex, weight));
                result = true;
            }
        }

        return result;
    }

    public boolean hasNeighbor() {
        return !edgeList.isEmpty();
    }

    protected class Edge {
        private Vertex endVertex;
        private double weight;

        protected Edge(Vertex endVertex, double weight) {
            this.endVertex = endVertex;
            this.weight = weight;
        }

        protected Vertex getEndVertex() {
            return endVertex;
        }

        protected double getWeight() {
            return weight;
        }

    }

    public static void main(String[] args) {

        Graph2 graph = setGapth();
        graph.depthFirstVisit('7');
        graph.resetVertices();
        graph.breadthFirstVisit('0');


        Stack<Vertex> pathStack = new Stack<Vertex>();

        double pathLength = graph.getShortestPath('0', '7', pathStack);
        System.out.println(pathLength);
        while (!pathStack.isEmpty()) {
            Vertex vertex = pathStack.pop();
            System.out.print(vertex.getLabel() + " ");
        }

    }

    public static Graph2 setGapth(){


        Graph2 graph=new Graph2();
        graph.addVertex('0');
        graph.addVertex('1');
        graph.addVertex('2');
        graph.addVertex('3');
        graph.addVertex('4');
        graph.addVertex('5');
        graph.addVertex('6');
        graph.addVertex('7');

        graph.addEdge('0','4');
        graph.addEdge('0','3');
        graph.addEdge('0','1');

        graph.addEdge('1','4');
        graph.addEdge('1','2');
        graph.addEdge('1','0');

        graph.addEdge('2','5');
        graph.addEdge('2','1');

        graph.addEdge('3','6');
        graph.addEdge('3','0');

        graph.addEdge('4','6');
        graph.addEdge('4','1');
        graph.addEdge('4','0');

        graph.addEdge('5','2');

        graph.addEdge('6','7');
        graph.addEdge('6','4');
        graph.addEdge('6','3');

        graph.addEdge('7','6');

        return graph;
    }
}