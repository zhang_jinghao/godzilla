
public class EmptyCollectionException extends Exception {

    public EmptyCollectionException(String message) {
        super("The " + message + " is empty.");
    }

}