import java.util.ArrayList;
import java.util.Arrays;

public class LinkedBinaryTree<T> implements BinaryTree3<T>
{
    private BTNode3<T> root;

    public LinkedBinaryTree()
    {
        root = null;
    }

    public LinkedBinaryTree(T element)
    {
        root = new BTNode3<T>(element);
    }

    public LinkedBinaryTree(T element, LinkedBinaryTree<T> left, LinkedBinaryTree<T> right) {
        root = new BTNode3<T>(element);
        root.setLeft(left.root);
        root.setRight(right.root);
    }

    public T getRootElement() {
        if (root == null)
            throw new EmptyCollectionException("Get root operation "
                    + "failed. The tree is empty.");

        return root.getElement();
    }

    public LinkedBinaryTree3<T> getLeft() {
        if (root == null)
            throw new EmptyCollectionException("Get left operation "
                    + "failed. The tree is empty.");

        LinkedBinaryTree<T> result = new LinkedBinaryTree<T>();
        result.root = root.getLeft();

        return result;
    }

    public T find (T target) {
        BTNode<T> node = null;

        if (root != null)
            node = root.find(target);

        if (node == null)
            throw new ElementNotFoundException("Find operation failed. "
                    + "No such element in tree.");

        return node.getElement();
    }

    public int size() {
        int result = 0;

        if (root != null)
            result = root.count();

        return result;
    }

    public ArrayList<T> inorder() {
        ArrayList<T> iter = new ArrayList<T>();

        if (root != null)
            root.inorder (iter);

        return iter;
    }

    public ArrayList<T> levelorder() {
        LinkedQueue3<BTNode<T>> queue = new LinkedQueue3<BTNode<T>>();
        ArrayList<T> iter = new ArrayList<T>();

        if (root != null)
        {
            queue.enqueue(root);
            while (!queue.isEmpty())
            {
                BTNode3<T> current = queue.dequeue();

                iter.add (current.getElement());

                if (current.getLeft() != null)
                    queue.enqueue(current.getLeft());
                if (current.getRight() != null)
                    queue.enqueue(current.getRight());
            }
        }

        return iter;
    }

    public LinkedBinaryTree3<T> getRight() {
        if (root == null)
            throw new EmptyCollectionException("Get left operation "
                    + "failed. The tree is empty.");

        LinkedBinaryTree3<T> result = new LinkedBinaryTree3<T>();
        result.root = root.getRight();

        return result; }

    public boolean contains (T target) {
        return root.find(target) != null;
    }

    public boolean isEmpty() {
        return root == null;
    }

    public String toString() {
        return super.toString();
    }

    public ArrayList<T> preorder() {
        ArrayList<T> iter = new ArrayList<T>();

        if (root != null)
            root.preorder (iter);

        return iter;
    }

    public ArrayList<T> postorder() {
        ArrayList<T> iter = new ArrayList<T>();

        if (root != null)
            root.postorder (iter);

        return iter;
    }

    private BTNode3 construct(char[] pre, char[] in){
        if (pre.length == 0 || in.length == 0) {
            return null;
        }
        BTNode3<Character> tree = new BTNode3<Character>(pre[0]);
        int index = search(0, in.length, in, tree.getElement());
        tree.setLeft(construct(Arrays.copyOfRange(pre, 1, index + 1), Arrays.copyOfRange(in, 0, index)));
        tree.setRight(construct(Arrays.copyOfRange(pre, index + 1, pre.length),
                Arrays.copyOfRange(in, index + 1, in.length)));
        return tree;
    }

    private int search(int start, int end, char[] inOrders, char data) {
        for (int i = start; i < end; i++) {
            if (data == inOrders[i]) {
                return i;
            }
        }
        return -1;
    }

    private void preOrder(BTNode3<T> Tree) {
        System.out.print(Tree.getElement()+" ");
        BTNode3<T> leftTree = Tree.left;
        if(leftTree != null)
        {
            preOrder(leftTree);
        }
        BTNode3<T> rightTree = Tree.right;
        if(rightTree != null)
        {
            preOrder(rightTree);
        }
    }

    public static void inOrder(BTNode3 tree) {
        if(tree == null) {
        }
        else
        {
            inOrder(tree.left);
            System.out.print(tree.print()+" ");
            inOrder(tree.right);
        }
    }
}