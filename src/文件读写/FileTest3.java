package com.company;
import java.io.*;
/**
 * Created  on 20119/10/13
 */
public class FileTest3 {
    public static void main(String[] args) throws IOException {
        //1、创建文件
        File file = new File("Test3.txt");
        //File file = new File("Test3.txt");
//        File file3 = new File("C:\Users\hp\IdeaProjects\untitled1\src");
//        file1.mkdir();
//        file1.mkdirs();
        if (!file.exists()){
            //2、文件读写
            //字节流读写，先写后读
            file.createNewFile();
        }
        Writer writer2 = new FileWriter(file);
        writer2.write("Hello, I'm Bill");
        writer2.flush();

        OutputStream outputStream2 = new FileOutputStream(file);
        BufferedOutputStream bufferedOutputStream2 = new BufferedOutputStream(outputStream2);
        String content2 = "BufferedOutputStream";
        bufferedOutputStream2.write(content2.getBytes(),0,content2.getBytes().length);
        bufferedOutputStream2.flush();
        bufferedOutputStream2.close();

        Reader reader2 = new FileReader(file);
        System.out.println("");
        while(reader2.ready()){
            System.out.print((char) reader2.read()+ "  ");
        }
    }
}










