class EdgeNode {
    private int adjvex;
    private int weight;
    private EdgeNode next;

    EdgeNode(int adjvex, int weight, EdgeNode next) {
        super();
        this.adjvex = adjvex;
        this.weight = weight;
        this.next = next;
    }

    int getAdjvex() {
        return adjvex;
    }

    public void setAdjvex(int adjvex) {
        this.adjvex = adjvex;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    EdgeNode getNext() {
        return next;
    }

    void setNext(EdgeNode next) {
        this.next = next;
    }
}