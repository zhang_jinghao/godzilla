import java.util.ArrayDeque;
import java.util.Collections;
import java.util.List;
import java.util.Queue;

public class HuffmanTree {
    static HuffmanNode createTree(List<HuffmanNode<String>> nodes) {
        while (nodes.size() > 1){
            Collections.sort(nodes);
            HuffmanNode left = nodes.get(nodes.size() - 2);
            left.setCode(0 + "");
            HuffmanNode right = nodes.get(nodes.size() - 1);
            right.setCode(1 + "");
            HuffmanNode parent;
            parent = new HuffmanNode(left.getWeight() + right.getWeight(), null);
            parent.setLeft(left);
            parent.setRight(right);
            nodes.remove(left);
            nodes.remove(right);
            boolean add;
            if (nodes.add(parent)) add = true;
            else add = false;
        }
        return nodes.get(0);
    }
    static List<HuffmanNode> BFS(HuffmanNode root){
        Queue<HuffmanNode> queue = new ArrayDeque<HuffmanNode>();
        List<HuffmanNode> list = new java.util.ArrayList<HuffmanNode>();

        if (root != null){
            queue.offer(root);
            root.getLeft().setCode(root.getCode() + "0");
            root.getRight().setCode(root.getCode() + "1");
        }

        while (!queue.isEmpty()){
            list.add(queue.peek());
            HuffmanNode node = queue.poll();
            assert node != null;
            if (node.getLeft() != null){
                queue.offer(node.getLeft());
                node.getLeft().setCode(node.getCode() + "0");
            }
            if (node.getRight() != null){
                queue.offer(node.getRight());
                node.getRight().setCode(node.getCode() + "1");
            }
        }
        return list;
    }
}
