import java.util.Scanner;

public class ASLTest {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        ASL asl = new ASL();
        int target = 0;
        int i,j=0;
        int[] arr = {19,14,23,1,68,20,84,27,55,11,10,79};
   System.out.println("顺序查找，输入要查找的数:");
        target = scan.nextInt();
        System.out.println("是否找到:"+asl.order(arr,target));

       System.out.println("折半查找，请输入:");
        target = scan.nextInt();
        System.out.println("是否找到:"+asl.binary(arr,0,11,5,target));
      System.out.println("线性探查法查找，请输入:");
        target = scan.nextInt();
        int result[] = asl.hash(arr);
        int a = asl.hashsearch(result,target);
        if(a==0)
            System.out.println("数组不存在此数");
        else
            System.out.println("数组中有此数");
     System.out.println("链地址法查找，请输入:");
        target = scan.nextInt();
        Linked[] linked = new Linked[12];
        for(i=0;i<12;i++)
            linked[i] = new Linked(arr[i]);
        Linked[] re1 = asl.linkedhash(linked);
        int b = asl.linkedsearch(re1,target);
        if(b==0)
            System.out.println("数组不存在此数");
        else
            System.out.println("数组中有此数");

    }
}


