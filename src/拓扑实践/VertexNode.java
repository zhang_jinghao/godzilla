public class VertexNode {
    private int in;
    private int data;
    private EdgeNode firstEdge;

    VertexNode(int in, int data, EdgeNode firstEdge) {
        super();
        this.in = in;
        this.data = data;
        this.firstEdge = firstEdge;
    }

    int getIn() {
        return in;
    }

    void setIn(int in) {
        this.in = in;
    }

    public int getData() {
        return data;
    }

    public void setData(int data) {
        this.data = data;
    }

    EdgeNode getFirstEdge() {
        return firstEdge;
    }

    void setFirstEdge(EdgeNode firstEdge) {
        this.firstEdge = firstEdge;
    }
}