
abstract public class StaffMember {
    protected String name;
    protected  String address;
    protected String phone;
    protected String vacation;
    public StaffMember(String name, String eName, String eAddress, String ePhone,String eVacation)
    {
        this.name =eName;
        address=eAddress;
        phone=ePhone;
        vacation=eVacation;

    }

    public StaffMember(String eName, String eAddress, String ePhone, int eVacation) {

    }

    public String toString()
    {
        String result="Name:"+name+"\n";
        result+="Address:"+address+"\n";
        result+="Phone:"+phone+"\n";
        result+="Vacation:"+vacation+"\n";
        return result;
    }
    public abstract double pay();
}
