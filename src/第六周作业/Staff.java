public class Staff {
    private StaffMember[] staffList;
    public Staff()
    {
        staffList =new StaffMember[4];
        staffList[0]=new Executive("Tony","123 xxx","43456","8654",123,1);
        staffList[1]=new Employee("Paulie","234 xxx","42340","3544",456,2);
        staffList[2]=new Employee("vito","567 xxx","45666","6776",788,3);
        staffList[3]=new Employee("Benny","890 xxx","98767","3454",890,4);

    }
    public void payday() {
        double amount;
        for (int count = 0; count < staffList.length; count++) {
            System.out.println(staffList[count]);
            amount = staffList[count].pay();

            System.out.println("Paid:" + amount);
        }
    }
}