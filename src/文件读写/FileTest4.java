package com.company;
import java.io.*;
/**
 * Created  on 20119/10/13
 */
public class FileTest4 {
    public static void main(String[] args) throws IOException {
        //1、文件创建
        File file = new File("Test4.txt");
        //File file = new File("Test3.txt");
//        File file4 = new File("C:\Users\hp\IdeaProjects\untitled1\src");
        if (!file.exists()){
           //2、文件读写
          //字节流读写，先写后读
            file.createNewFile();
        }

        byte[] test2 = {'A','r','c','h','e','r'};
        byte[] buffer = new byte[8];
        OutputStream outputStream1 = new FileOutputStream(file);
        outputStream1.write(test2);
        String content = "";

        Reader reader2 = new FileReader(file);
        BufferedReader bufferedReader = new BufferedReader(reader2);
        System.out.println("\n下面是用BufferedReader读出的数据");
        while ((content =bufferedReader.readLine())!= null){
            System.out.println(content);
        }
    }
}

























