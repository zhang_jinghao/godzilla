package com.company;
import java.io.*;
/**
 * Created  on 20119/10/13
 */
public class FileTest2 {
    public static void main(String[] args) throws IOException {
        //1、创建文件
        File file = new File("Test2.txt");
        //File file = new File("Test2.txt");
//        File file2 = new File("C:\Users\hp\IdeaProjects\untitled1\src");
    if (!file.exists()){
            //2、文件读写
            //字节流读写，先写后读
            file.createNewFile();
        }
        OutputStream outputStream1 = new FileOutputStream(file);
     int flag = 0;
     String content = "";
        byte[] test2 = {'A','c','h','e','r'};
        byte[] buffer = new byte[8];
        outputStream1.flush();//可有可无
        outputStream1.write(test2);

        InputStream inputStream2 = new FileInputStream(file);
        BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream2);

        while ((flag =bufferedInputStream.read(buffer))!=-1){
            content += new String(buffer,0,flag);
        }

        System.out.println(content);
        bufferedInputStream.close();
    }
}










