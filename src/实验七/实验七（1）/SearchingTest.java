import junit.framework.TestCase;
import org.junit.Test;
public class SearchingTest extends TestCase {
    @Test
        public void testLinearSearch1() throws Exception{
        Comparable test[] = {20, 18, 11, 19, 2328};
        assertEquals(true, Searching.linearSearch(test, 20));
        assertEquals(true, Searching.linearSearch(test, 2328));
        assertEquals(true, Searching.linearSearch(test, 18));
        assertEquals(false, Searching.linearSearch(test, 100));
    }
    @Test
    public void testLinearSearch2() throws Exception{
        Comparable test2[] = {1, 2, 3, 4, 5};
        assertEquals(true, Searching.linearSearch(test2, 1));
        assertEquals(true, Searching.linearSearch(test2, 5));
        assertEquals(true, Searching.linearSearch(test2, 3));
        assertEquals(false, Searching.linearSearch(test2, 100));
    }
    @Test
    public void testLinearSearch3() throws Exception{
        Comparable test3[] = {5, 4, 3, 2, 1};
        assertEquals(true, Searching.linearSearch(test3, 1));
        assertEquals(true, Searching.linearSearch(test3, 5));
        assertEquals(true, Searching.linearSearch(test3, 3));
        assertEquals(false, Searching.linearSearch(test3, 100));
    }
    @Test
    public void testLinearSearch4() throws Exception{
        Comparable test4[] = {2, 0, 1, 8, 2, 3, 2, 8};
        assertEquals(true, Searching.linearSearch(test4, 2));
        assertEquals(true, Searching.linearSearch(test4, 8));
        assertEquals(true, Searching.linearSearch(test4, 8));
        assertEquals(false, Searching.linearSearch(test4, 100));
    }
    @Test
    public void testLinearSearch5() throws Exception{
        Comparable test5[] = {2, 2, 3, 8};
        assertEquals(true, Searching.linearSearch(test5, 2));
        assertEquals(true, Searching.linearSearch(test5, 8));
        assertEquals(true, Searching.linearSearch(test5, 2));
        assertEquals(false, Searching.linearSearch(test5, 100));
    }
    @Test
    public void testLinearSearch6() throws Exception{
        Comparable test6[] = {8, 3, 2, 2};
        assertEquals(true, Searching.linearSearch(test6, 8));
        assertEquals(true, Searching.linearSearch(test6, 2));
        assertEquals(true, Searching.linearSearch(test6, 3));
        assertEquals(false, Searching.linearSearch(test6, 100));
    }
    }
}