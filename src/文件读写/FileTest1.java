package com.company;
import java.io.*;
/**
 * Created  on 20119/10/13
 */
public class FileTest1
{
    public static void main(String[] args) throws IOException {
        //1、创建文件（文件类实例化）
        File file = new File("Test1.txt");
        //File file = new File("Test1.txt");
//        File file1 = new File("C:\Users\hp\IdeaProjects\untitled1\src");
//        file1.mkdir();
//        file1.mkdirs();
     if (!file.exists()){
            //2、文件读写
            //字节流读写，先写后读
            file.createNewFile();
        }
        OutputStream outputStream1 = new FileOutputStream(file);
      byte[] test1 = {'s','a','b','e','r'};
      outputStream1.write(test1);
      outputStream1.flush();//可以添加，也可以不添加
      InputStream inputStream1 = new FileInputStream(file);
        while (inputStream1.available()> 0){
            System.out.print((char) inputStream1.read()+"  ");
        }
        inputStream1.close();
    }
}






















