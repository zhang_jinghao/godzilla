public class SearchingTest {
    public static void main(String[] args) {
        int []a={1,3,4,23,28,12,67};
        String array="";

        for (int i=0;i<a.length;i++)
            array+=a[i]+" ";

        System.out.println("原数组为："+array);

        System.out.println("用线性查找法寻找28："+Searching.SequenceSearch(a,28,7));
        System.out.println("用二分法查找法寻找28："+Searching.BinarySearch1(a,28,7));


        System.out.println("用斐波那契查找法寻找23："+Searching.FibonacciSearch(a,7,67));

        System.out.println("用树表查找法寻找23："+Searching.TreeSearch(a,23));

        System.out.println("用分块查找法寻找23："+Searching.BlockSearch(a,23,new int[]{10,20,70}));

    }
}