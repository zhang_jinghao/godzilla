import java.util.*;

/*
 * 用来实现拓扑排序的有向无环图
 */
public class Topology {

    private class Vertex{
        private String vertexLabel;// 作为顶点的标志
        private List<Edge> adjEdges;//定义一个列表
        private int inDegree;// 定义顶点的入度
        Vertex(String verTtexLabel) {
            this.vertexLabel = verTtexLabel;
            inDegree = 0;
            adjEdges = new LinkedList<Edge>();
        }
    }
    private class Edge {
        private Vertex endVertex;
        Edge(Vertex endVertex) {
            this.endVertex = endVertex;
        }
    }
    private Map<String, Vertex> directedGraph;

    Topology() {
        directedGraph = new LinkedHashMap<String, Topology.Vertex>();
        Graph();
    }
/*
    private void Graph(){

        Vertex startNode, endNode;
        String startNodeLabel, endNodeLabel;
        Edge e;
        Scanner scan = new Scanner(System.in);
        System.out.println("请输入有向图的边数：");
        int m = scan.nextInt();

        for (int i = 0; i <m; i++) {
            System.out.println("请输入第"+(i+1)+"边的头结点和尾结点： ");
            startNodeLabel = scan.next();
            endNodeLabel = scan.next();
            startNode = directedGraph.get(startNodeLabel);
            if (startNode == null) {
                startNode = new Vertex(startNodeLabel);
                directedGraph.put(startNodeLabel, startNode);
            }
            endNode = directedGraph.get(endNodeLabel);
            if (endNode == null) {
                endNode = new Vertex(endNodeLabel);
                directedGraph.put(endNodeLabel, endNode);
            }

            e = new Edge(endNode);
            startNode.adjEdges.add(e);
            endNode.inDegree++;
        }

    }

    void topoSort() throws Exception{
        int count = 0;
        Queue<Vertex> queue = new LinkedList<>();
        Collection<Vertex> vertexs = directedGraph.values();
        for (Vertex vertex : vertexs)
            if(vertex.inDegree == 0)
                queue.offer(vertex);
        System.out.println("拓扑排序序列为：");
        while(!queue.isEmpty()){
            Vertex v = queue.poll();
            System.out.print(v.vertexLabel + " ");
            count++;
            for (Edge e : v.adjEdges)
                if(--e.endVertex.inDegree == 0)
                    queue.offer(e.endVertex);
        }
        if(count != directedGraph.size()){
            throw new Exception("该图有环！");
        }
    }
}
*/
