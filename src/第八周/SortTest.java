public class SortTest {
    public static void main(String[] args) {

        String[] list = {"I", "am", "zjh", "2018", "2328"};
        Integer[] list1 = {1, 2, 3, 4, 5, 6, 7, 8};
        Double[] list2 = {2.3,4.4,1.5,2.6,8.7,6.8};
        System.out.println("在同一列表上执行这些排序算法");
        System.out.println("选择排序：");
        Sort.selectionSort(list2);
        System.out.println();
        System.out.println("插入排序：");
        Sort.charuSort(list2);
        System.out.println();
        System.out.println("冒泡排序：");
        Sort.maopaoSort(list2);
        System.out.println();
        System.out.println("快速排序：");
        Sort.quickSort(list2);
        System.out.println();
        System.out.println("归并排序：");
        Sort.guibingSort(list2);
        System.out.println();

        for(double number:list2)
            System.out.print(number+" ");

        System.out.println();
        System.out.println("快速排序：");
        Sort.quickSort(list);
        for(String line:list)
            System.out.print(line+"\t");
        System.out.println();

        System.out.println("归并排序：");
        Sort.guibingSort(list1);
        for(int number:list1)
            System.out.print(number+"\t");
    }
}