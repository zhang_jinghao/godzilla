public class HuffmanNode<T> implements Comparable<HuffmanNode<T>>{
    private double weight;
    private T word;
    private HuffmanNode left;
    private HuffmanNode right;
    private HuffmanNode parent;
    private String code;

    HuffmanNode(double weight, T word){
        this.weight = weight;
        this.word = word;
        this.code = "";
    }

    String getCode() {
        return code;
    }

    void setCode(String str) {
        code = str;
    }

    double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    T getWord() {
        return word;
    }

    public void setWord(T word) {
        this.word = word;
    }

    HuffmanNode getLeft() {
        return left;
    }

    void setLeft(HuffmanNode left) {
        this.left = left;
    }

    HuffmanNode getRight() {
        return right;
    }

    void setRight(HuffmanNode right) {
        this.right = right;
    }

    public HuffmanNode getParent() {
        return parent;
    }

    public void setParent(HuffmanNode parent) {
        this.parent = parent;
    }

    @Override
    public String toString() {
        return word + " 权值： " + weight + " 左孩子： " + left + " 右孩子： " + right;
    }

    @Override
    public int compareTo(HuffmanNode<T> o) {
        if (this.getWeight() > o.getWeight()){
            return -1;
        }
        else if (this.getWeight() < o.getWeight()){
            return 1;
        }
        return 0;
    }
}
