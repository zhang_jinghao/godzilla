import java.util.Scanner;

public class BinarySearchTree {
    public static void main(String[] args) {
        int target = 0;
        Scanner scan = new Scanner(System.in);
        BinaryTree binaryTree1 = new BinaryTree();
        binaryTree1.data = 19;

        BinaryTree binaryTree2 = new BinaryTree();
        binaryTree1.lchild = binaryTree2;
        binaryTree2.data = 14;

        BinaryTree binaryTree3 = new BinaryTree();
        binaryTree2.lchild = binaryTree3;
        binaryTree3.data = 23;

        BinaryTree binaryTree4 = new BinaryTree();
        binaryTree3.lchild = binaryTree4;
        binaryTree4.data = 1;

        BinaryTree binaryTree5 = new BinaryTree();
        binaryTree4.rchild = binaryTree5;
        binaryTree5.data = 68;

        BinaryTree binaryTree6 = new BinaryTree();
        binaryTree3.rchild = binaryTree6;
        binaryTree6.data = 20;

        BinaryTree binaryTree7 = new BinaryTree();
        binaryTree1.rchild = binaryTree7;
        binaryTree7.data = 84;

        BinaryTree binaryTree8 = new BinaryTree();
        binaryTree7.lchild = binaryTree8;
        binaryTree8.data = 27;

        BinaryTree binaryTree9 = new BinaryTree();
        binaryTree7.rchild = binaryTree9;
        binaryTree9.data = 55;

        BinaryTree binaryTree10 = new BinaryTree();
        binaryTree9.lchild = binaryTree10;
        binaryTree10.data = 11;

        BinaryTree binaryTree11 = new BinaryTree();
        binaryTree9.lchild = binaryTree10;
        binaryTree10.data = 10;

        BinaryTree binaryTree12 = new BinaryTree();
        binaryTree9.lchild = binaryTree10;
        binaryTree10.data = 10;
        System.out.println("二叉排序树查找，请输入:");
        target = scan.nextInt();
        boolean search = serachBinaryTree(binaryTree1, 37, null);
    }
    static BinaryTree parentNode = new BinaryTree();

    public static boolean serachBinaryTree(BinaryTree bt, int key, BinaryTree parent) {
        if (bt == null || bt.data == 0) {
            parentNode = parent;
            return false;
        } else if (key == bt.data) {
            parentNode = bt;
            return true;
        } else if (key < bt.data) {
            return serachBinaryTree(bt.lchild, key, bt);
        } else {
            return serachBinaryTree(bt.rchild, key, parent);
        }
    }
 static class BinaryTree {
        int data;
        BinaryTree lchild;
        BinaryTree rchild;
    }
}