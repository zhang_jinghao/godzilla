import java.util.Arrays;
public class ArrayStack<T> implements Stack<T> {
    private final int DEFAULT_CAPACITY = 10;

    private int top;
    private T[] stack;

    public ArrayStack() {
        top = 0;
        stack = (T[]) (new Object[DEFAULT_CAPACITY]);

    }

    public ArrayStack(int initialCapacity){
        top= 0;
        stack = (T[])(new Object[initialCapacity]);

    }


    public T peek()throws EmptyCollectionException
    {
        if(isEmpty()) {
            throw new EmptyCollectionException("Stack");
        }
        return stack[top-1];
    }


    public boolean isEmpty(){
        if(Size() == 0) {
            return true;
        }
        else
        {
            return false;
        }
    }

    public int Size(){
        return top ;
    }

    public void push (T element)
    {
        if (Size() == stack.length)
            expandCapacity();
        stack[top] = element;
        top++;

    }

    public void expandCapacity(){
        stack = Arrays.copyOf(stack, stack.length * 2);

    }
    public T pop() throws EmptyCollectionException
    {
        if(isEmpty()) {
            throw new EmptyCollectionException("Stack");
        }
        top--;
        T result = stack[top];
        stack[top] = null;

        return result;

    }



    public String toString()
    {
        String line = "";

        for(int i = 0 ; i <top; i++)
        {
            line +=  stack[i]+"";
        }
        return line;
    }
}