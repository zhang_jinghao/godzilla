
import java.util.*;

public class Graph {
    private int[][] matrix;
    private char[] verx;
    private int num1;
    private int num2;
    private Queue list;
    private int[] isVisited;

    public Graph(int num1, int num2) {
        this.num1 = num1;
        this.num2 = num2;
        this.matrix = new int[num1][num2];
        this.verx = new char[num1];
        this.list = new LinkedList();
        this.isVisited = new int[num1];
    }

    public void setGraph() {
        Scanner scan = new Scanner(System.in);
        System.out.println("输入顶点：");
        for (int i = 0; i < num1; i++) {
            verx[i] = scan.next().charAt(0);
        }
        for (int i = 0; i < num1; i++) {
            for (int j = 0; j < num1; j++) {
                matrix[i][j] = 0;
            }
        }
        for (int k = 0; k < num2; k++) {
            System.out.println("输入头尾：");
            char a = scan.next().charAt(0);
            char b = scan.next().charAt(0);
            int i = this.test(a);
            int j = this.test(b);
            this.matrix[i][j] = 1;
        }

    }

    private int test(char a) {
        for (int i = 0; i < num1; i++) {
            if (verx[i] == a)
                return i;
        }
        return -1;
    }

    public void DFSTraverse(int i) {
        DFS(this, i);
        for (int j = 0; j < isVisited.length; j++)
            isVisited[j] = 0;
    }

    private void DFS(Graph a, int i) {
        isVisited[i] = 1;
        System.out.println(verx[i]);
        for (int j = 0; j < num1; j++) {
            if (matrix[i][j] == 1 && isVisited[j] == 0) {
                DFS(a, j);
            }
        }
    }

    public void BFST(int i) {
        final boolean add = list.add(verx[i]);
        BFS();
        for (int j = 0; j < isVisited.length; j++)
            isVisited[j] = 0;
    }

    private void BFS() {
        char temp = (char) list.poll();
        int x = temp - '0';
        isVisited[x] = 1;
        System.out.println(temp);
        List nerborpointlist = getCurrent(x);
        for (Object o : nerborpointlist) {
            char j = (char) o;
            final boolean add = list.add(j);
            int k = j - '0';
            isVisited[k] = 1;
        }
        if (!list.isEmpty()) {
            BFS();
        }
    }

    private List getCurrent(int i) {
        List list1 = new LinkedList();
        for (int j = 0; j < num1; j++) {
            if (matrix[i][j] == 1 && isVisited[j] == 0) {
                list1.add(verx[j]);
            }
        }
        return list1;
    }

    public void delete(char a, char b) {
        int i = test(a);
        int j = test(b);
        matrix[i][j] = 0;
    }

    public void add(char a) {
        char[] temp = Arrays.copyOf(verx, num1 + 1);
        temp[num1] = a;
        verx = Arrays.copyOf(temp, temp.length);
        num1++;
    }

    public void delete(char a) {
        char[] temp = new char[num1 - 1];
        int j = 0;
        for (int i = 0; i < num1; i++) {
            if (verx[i] != a)
                temp[j++] = verx[i];
        }
        verx = Arrays.copyOf(temp, temp.length);
        num1--;
    }

    public int size() {
        return num1;
    }

    public boolean isEmpty() {
        return num1 == 0;
    }
}


