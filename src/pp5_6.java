public class pp5_6 {
    private int h;
   private  double w;
   private int d;
   private boolean full;

    public pp5_6(int h, double w, int d){
        this.h=h;
        this.w=w;
        this.d=d;
        full=false;
    }
    public int getH(){
        return h;
    }
    public void setH(int h){
        h=this.h;

    }
    public  double getW(){
        return w;
    }
    public void setW(double w){
        w=this.w;
    }
    public int getD() {
        return d;
    }
    public void setD(int d) {
        d=this.d;
    }
    public boolean isFull() {
        return full;
    }

    public void setFull(boolean full) {
         full=this.full;
    }

    public String toString(){
        return "Box(" + "h=" + h + " w=" + w + " d=" + d + " full=" + full + ')';
    }
}
