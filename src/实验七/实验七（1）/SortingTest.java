import junit.framework.TestCase;
import static org.junit.Assert.assertNotEquals;
public class SortingTest extends TestCase {
    public void testselectionSort() throws Exception {
        String expect = 11+" "+18+" "+19+" "+20+" "+2328+" ";
        String expect2 = 1+" "+2+" "+3+" "+4+" "+5+" ";
        String expect3 = 5+" "+4+" "+3+" "+2+" "+1+" ";
        String expect4 = "B"+" "+"D"+" "+"E"+" "+"G"+" "+"C"+" ";
        String expect5 = "A"+" "+"B"+" "+"C"+" "+"D"+" "+"E"+" ";
        String expect6 = "E"+" "+"D"+" "+"C"+" "+"B"+" "+"A"+" ";
        String expect7 = "B"+" "+"C"+" "+"D"+" "+"E"+" "+"G"+" ";


        Comparable test[] = {11,18,19,20,2328};
        assertEquals(expect,Sorting.selectionSort(test));
        assertNotEquals(expect2,Sorting.selectionSort(test));

        Comparable test2[] = {1,2,3,4,5};
        assertEquals(expect2,Sorting.selectionSort(test2));
        assertNotEquals(expect,Sorting.selectionSort(test2));

        Comparable test3[] = {5,4,3,2,1};
        assertEquals(expect2,Sorting.selectionSort(test3));
        assertNotEquals(expect,Sorting.selectionSort(test3));

        Comparable test4[] = {'B','D','E','G','C'};
        assertEquals(expect4,Sorting.selectionSort(test4));
        assertNotEquals(expect3,Sorting.selectionSort(test4));

        Comparable test5[] = {'A','B','C','D','E'};
        assertEquals(expect4,Sorting.selectionSort(test5));
        assertNotEquals(expect3,Sorting.selectionSort(test5));

        Comparable test6[] = {'E','D','C','B','A'};
        assertEquals(expect4,Sorting.selectionSort(test6));
        assertNotEquals(expect3,Sorting.selectionSort(test6));

        Comparable test7[] = {'B','C','D','E','G'};
        assertEquals(expect4,Sorting.selectionSort(test6));
        assertNotEquals(expect3,Sorting.selectionSort(test6));
    }


}