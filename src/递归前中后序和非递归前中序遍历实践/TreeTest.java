import java.util.LinkedList;
import java.util.Scanner;
import java.util.Stack;

/**
 *
 */
class Count{
    static int count=0;
}
public class TreeTest{
    private static TreeNode createTree(TreeNode root, String[] a, int i)	{

        if(i < a.length)
        {
            if(a[i].equals("#"))
            {
                root = null;
            }
            else
            {
                TreeNode left = new TreeNode();
                TreeNode right = new TreeNode();
                root.setNode(  a[i]  ,  createTree(left,a,++Count.count) ,  createTree(right,a,++Count.count)  );
            }
        }		return root;
    }


    private static void traverse(TreeNode root)	{
        if(root != null)
        {
            System.out.print(root.getData()+" ");
            traverse(root.getLchild());
            traverse(root.getRchild());
        }else{
            System.out.print("-- ");
        }
    }

    private static  void  preOrderTraverse(TreeNode root){
        if(root != null){
            System.out.print(root.getData());
            if(root.getLchild() != null){
                preOrderTraverse(root.getLchild());
            }
            if(root.getRchild() != null){
                preOrderTraverse(root.getRchild());
            }
        }else System.out.print("-");
    }

    private static void Middle(TreeNode root){
        if(root != null){

            if(root.getLchild() != null){
                Middle(root.getLchild());
            }
            System.out.print(root.getData() + " ");
            if(root.getRchild() != null){
                Middle(root.getRchild());
            }
        }else System.out.print("-");
    }

    private static void front(TreeNode root){
        if(root != null){

            if(root.getLchild() != null){
                Middle(root.getLchild());
            }

            if(root.getRchild() != null){
                Middle(root.getRchild());
            }
            System.out.print(root.getData() + " ");
        }else System.out.print("-");
    }

    public static void levelOrderTraverse(TreeNode root){
        TreeNode temp = root;
        LinkedList linkedList = new LinkedList();
        linkedList.add(temp);
        while(!linkedList.isEmpty()){
            temp = linkedList.poll();
            System.out.print(temp.data + " ");
            if(temp.getLchild() != null ) {
                final boolean add = linkedList.add(temp.getLchild());
            }
            if(temp.getRchild() != null){
                final boolean add;
                add = linkedList.add(temp.getRchild());
            }
        }
    }
    private static void preOrderStack2(TreeNode root) {
        Stack<TreeNode> stack = new Stack<>();
        TreeNode treeNode = root;
        while (treeNode != null || !stack.isEmpty()) {
            //将左孩子点循环不断的压入栈
            while (treeNode != null) {
                //进行访问再入栈
                System.out.print(treeNode.data + " ");
                stack.push(treeNode);
                treeNode = treeNode.left;
            }
            //出栈并处理右孩子数
            if (!stack.isEmpty()) {
                treeNode = stack.pop();
                treeNode = treeNode.right;
            }

        }

    }

    private static void inOrderStack(TreeNode treeNode) {
        Stack<TreeNode> stack = new Stack<>();
        while (treeNode != null || !stack.isEmpty()) {
            while (treeNode != null) {
                stack.push(treeNode);
                treeNode = treeNode.left;
            }
            //左孩子数完全进栈
            if (!stack.isEmpty()) {
                treeNode = stack.pop();
                System.out.print(treeNode.data + " ");
                treeNode = treeNode.right;
            }
        }
    }



    public static void main(String[] args)
    {
        private static char[] arr = {"A","B","#","C","D","#","#","#","E","#","F","#","#"};
        private static int count= 0;


        TreeNode root = new TreeNode();
        root = createTree(root,arr,0);
        if(root == null)
        {
            System.out.println("null");
        }
        System.out.println("递归先序遍历：");
        traverse(root);
        System.out.println();

        System.out.println("递归中序遍历：");
        Middle(root);
        System.out.println();

        System.out.println("递归后序遍历：");
        front(root);
        System.out.println();

        System.out.println("非递归先序遍历：");
        preOrderStack2(root);
        System.out.println();

        System.out.println("非递归中序遍历：");
        inOrderStack(root);
        System.out.println();
    }

}