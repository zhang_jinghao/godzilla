import java.util.*;
public class Prim {
    private static int MAX = Integer.MAX_VALUE;

    public static void main(String[] args) {
        int[][] map = new int[][]{
                {0, 11, MAX, MAX, MAX, 12, MAX, MAX, MAX},

                {10, 0, 19, MAX, MAX, MAX, 15, MAX, 10},

                {MAX, MAX, 0, 22, MAX, MAX, MAX, MAX, 8},

                {MAX, MAX, 22, 0, 20, MAX, MAX, 16, 21},

                {MAX, MAX, MAX, 20, 0, 18, MAX, 9, MAX},

                {11, MAX, MAX, MAX, 26, 0, 17, MAX, MAX},

                {MAX, 16, MAX, MAX, MAX, 17, 0, 16, MAX},

                {MAX, MAX, MAX, 20, 9, MAX, 20, 0, MAX},

                {MAX, 12, 8, 22, MAX, MAX, MAX, MAX, 1}};
        prim(map, map.length);
    }

/*
    private static void prim(int[][] graph, int n){
        char[] c = new char[]{'a','b','c','d','e','f','g','e','f'};
        int[] lowcost = new int[n];
        int[] mid= new int[n];
        int i, j, min, minid , sum = 0;
        for(i=1;i<n;i++)
        {
            lowcost[i]=graph[0][i];
            mid[i]=0;
        }
        List<Character> list = new ArrayList<Character>();
        list.add(c[0]);
        for(i=1;i<n;i++)
        {
            min=MAX;
            minid=0;
            for(j=1;j<n;j++)
            {
                if(lowcost[j]!=0&&lowcost[j]<min)
                {
                    min=lowcost[j];
                    minid=j;
                }
            }
            if(minid==0) return;
            list.add(c[minid]);
            lowcost[minid]=0;
            sum+=min;
            System.out.println(c[mid[minid]] + "到" + c[minid] + " 权值：" + min);
            for(j=1;j<n;j++) {
                if (lowcost[ j ] != 0 && lowcost[ j ] > graph[ minid ][ j ]) {
                    lowcost[ j ] = graph[ minid ][ j ];
                    mid[ j ] = minid;
                }
            }
        }
        System.out.println("sum:" + sum);
    }
}
*/
