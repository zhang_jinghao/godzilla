public class pp5_3{

    private enum pdd {Heads,Tails};
    private pdd a = pdd.Heads;
    private pdd b = pdd.Tails;

    public pp5_3()
    {
        flip();
    }
    public void  flip(){
        if((int) (Math.random() * 2)==0){
            a=pdd.Heads;
        }
        else {
            a=pdd.Tails;
        }
    }
    public   boolean isHeads(){
        return a.equals(b);
    }
    public String toString()
    {
        return (a.equals(b)) ? "Heads" : "Tails";
    }
}