import java.util.ArrayList;
public class Searching
{

    public static <T> boolean linearSearch(T[] data, int min, int max, T target)
    {
        int index = min;
        boolean found = false;

        while (!found && index <= max) {
            found = data[index].equals(target);
            index++;
        }
        return found;
    }

    public static <T extends Comparable<T>> boolean binarySearch(T[] data, int min, int max, T target)
    {
        boolean found = false;
        int midpoint = (min + max) / 2;

        if (data[midpoint].compareTo(target) == 0)
            found = true;

        else if (data[midpoint].compareTo(target) > 0)
        {
            if (min <= midpoint - 1)
                found = binarySearch(data, min, midpoint - 1, target);
        }

        else if (midpoint + 1 <= max)
            found = binarySearch(data, midpoint + 1, max, target);

        return found;
    }
    public static int SequenceSearch(int a[], int target, int n){
        int i;
        for (i = 0; i < n; i++){
            if (a[i] == target )
                return i;

        }
        return -1;
    }

    public static int BinarySearch1(int a[], int value, int n)
    {
        int low, high, mid;
        low = 0;
        high = n-1;
        while(low <= high)
        {
            mid = (low+high)/2;
            if(a[mid] == value)
                return mid;
            if(a[mid] > value)
                high = mid - 1;
            if(a[mid] < value)
                low = mid+1;
        }
        return -1;
    }

    public static int BinarySearch2(int a[], int value, int low, int high)
    {
        int mid = low+(high-low)/2;
        if(a[mid]==value)
            return mid;
        if(a[mid]>value)
            return BinarySearch2(a, value, low, mid-1);
        if(a[mid]<value)
            return BinarySearch2(a, value, mid+1, high);
        return -1;
    }



    public static boolean InsertionSearch(int a[], int value){
        if (InsertionSearch(a,value,0,a.length-1)==value){
            return true;
        }
        else {
            return false;
        }

    }
    private static int InsertionSearch(int a[], int value, int low, int high){
        int result=0;
        int mid = low+(value-a[low])/(a[high]-a[low])*(high-low);
        if(a[mid]==value) {
            result= a[mid];
        }
        if(a[mid]>value) {
            return InsertionSearch(a, value, low, mid-1);
        }
        if(a[mid]<value) {
            return InsertionSearch(a, value, mid+1, high);
        }
        return result;
    }


    public static int FibonacciSearch(int[] a, int n, int key)
    {
        int low = 0;
        int high=n-1;
        int max = 20;
        int[] F = new int[max];
        F[0] = 1;
        F[1] = 1;
        for (int i = 2; i < max; i++){
            F[i] = F[i - 1] + F[i - 2];
        }

        int k=0;
        while(n > F[k]-1)
            k++;

        int[] temp;
        temp = new int [F[k]-1];
        for (int x = 0; x < a.length; x++){
            temp[x] = a[x];
        }

        for(int i=n;i<F[k]-1;++i)
            temp[i]=a[n-1];

        while(low <= high)
        {
            int mid = low + F[k-1] - 1;
            if(key<temp[mid])
            {
                high = mid - 1;
                k -= 1;
            }
            else if(key > temp[mid])
            {
                low = mid + 1;
                k -= 2;
            }
            else
            {
                if(mid < n)
                    return mid;
                else
                    return n-1;
            }
        }
        return -1;
    }

    public static boolean  TreeSearch(int[] a ,int value){
        LinkedBinarySearchTree c =new LinkedBinarySearchTree();
        for (int i =0;i<a.length;i++){
            c.addElement(a[i]);
        }
        if (c.find(value).equals(value)){
            return true;
        }
        else
            return false;

    }

    public static int hashsearch(int[] hashTable, int target) {
        int hashAddress = hash(hashTable, target);
        while (hashTable[hashAddress] != target) {
            hashAddress = (++hashAddress) % hashTable.length;
            if (hashTable[hashAddress] == 0 || hashAddress == hash(hashTable, target)) {
                return -1;
            }
        }
        return hashAddress;
    }
    private static int hash(int[] hashTable, int target) {
        return target % hashTable.length;
    }
    private static void hashinsert(int[] hashTable, int target) {
        int hashAddress = hash(hashTable, target);
        while (hashTable[hashAddress] != 0) {
            hashAddress = (++hashAddress) % hashTable.length;
        }
        hashTable[hashAddress] = target;
    }

    public static boolean BlockSearch(int[] a,int target, int[] index) {

        ArrayList[] list;
        if (null != index && index.length != 0) {

            list = new ArrayList[index.length];
            for (int i = 0; i < list.length; i++) {
                list[i] = new ArrayList();
            }
        }
        else{
            throw new Error("index cannot be null or empty");
        }

        for (int i =0;i<a.length;i++){
            int c = binarySearching(index,a[i]);
            list[c].add(a[i]);
        }

        int i= binarySearching(index,target);
        for(int j=0;j<list[i].size();j++)
        {
            if(target==(int)list[i].get(j))
            {
                return true;
            }
        }
        return false;
    }
    private static int binarySearching(int[] index,int value){
        int start = 0,end =index.length;int mid = -1;
        while(start<=end){
            mid=(start+end)/2;
            if(index[mid]>value){
                end = mid -1;
            }else{

                start = mid+1;
            }
        }
        return start;
    }


}
