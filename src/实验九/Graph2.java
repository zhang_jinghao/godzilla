import java.util.ArrayList;
public class Graph2 {
    public ArrayList<String> vertexList;//存储图的节点
    public int[][] edges;
    public int numofEdges;
    public int n;
    Graph(int n) {
        vertexList = new ArrayList<String>(n);
        edges = new int[n][n];
        numofEdges = 0;
        this.n = n;
    }
    //初始化矩阵
    public int getNumofVertex() {
        return vertexList.size();
    }
    //节点的个数
    public int getNunofEdges() {
        return numofEdges;
    }
    //边的个数
    public int getWeight(String temp1, String temp2) {
        int i = vertexList.indexOf(temp1);
        int j = vertexList.indexOf(temp2);
        return edges[i][j];
    }
    //返回两个节点之间的权值
    void InsertVertex(String vertex) {
        vertexList.add(vertex);
    }
    //插入节点的方法
    public void RemoveVertex(String vertex){
        vertexList.remove(vertex);
    }
    //删除节点的方法
    void InsertEdge(int v1, int v2, int weight) {
        edges[v1][v2] = weight;
        edges[v2][v1] = weight;
        numofEdges++;
    }
    //插入边的方法
    public void deleteEdge(int v1, int v2) {
        edges[v1][v2] = 0;
        numofEdges--;
    }

    int wuxiangtu(int a) {
        int result = 0;
        for (int i = 0; i < n; i++) {
            result += edges[a][i];
        }
        return result;
    }

    void InsertEdge2(int v1, int v2, int weight) {
        edges[v1][v2] = weight;
        numofEdges++;
    }
/*
    public int youxiangtu(int a)
    {
        int result = 0;
        for (int i = 0; i < n; i++) {
            result += edges[a][i];
        }
        for(int i=0;i<n;i++)
        {
            result +=edges[i][a];
        }
        return  result;
    }
}

 */