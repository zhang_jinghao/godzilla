import java.util.ArrayList;

public class LinkedBinaryTree2<T> implements BinaryTree<T>
{
    protected BTNode<T> root;

    public LinkedBinaryTree2()
    {
        root = null;
    }

    public LinkedBinaryTree2(T element)
    {
        root = new BTNode<T>(element);
    }

    public LinkedBinaryTree2(T element, LinkedBinaryTree2<T> left,
                            LinkedBinaryTree2<T> right)
    {
        root = new BTNode<T>(element);
        root.setLeft(left.root);
        root.setRight(right.root);
    }

    public T getRootElement()
    {
        if (root == null)
            throw new EmptyCollectionException ("Get root operation "
                    + "failed. The tree is empty.");

        return root.getElement();
    }

    public LinkedBinaryTree2<T> getLeft()
    {
        if (root == null)
            throw new EmptyCollectionException ("Get left operation "
                    + "failed. The tree is empty.");

        LinkedBinaryTree2<T> result = new LinkedBinaryTree2<T>();
        result.root = root.getLeft();

        return result;
    }

    public LinkedBinaryTree2<T> getRight()
    {
        if (root == null)
            throw new EmptyCollectionException ("Get right operation "
                    + "failed. The tree is empty.");

        LinkedBinaryTree2<T> result = new LinkedBinaryTree2<T>();
        result.root = root.getRight();

        return result;
    }


    @Override
    public boolean contains(T target) {
        BTNode a=root.find(target);
        if(a!=null){
            return true;
        }
        return false;
    }


    public T find (T target)
    {
        BTNode<T> node = null;

        if (root != null)
            node = root.find(target);

        if (node == null)
            throw new ElementNotFoundException("Find operation failed. "
                    + "No such element in tree.");

        return node.getElement();
    }


    public int size()
    {
        int result = 0;

        if (root != null)
            result = root.count();

        return result;
    }


    public ArrayList<T> inorder()
    {
        ArrayList<T> iter = new ArrayList<T>();

        if (root != null)
            root.inorder(iter);

        return iter;
    }


    public ArrayList<T> levelorder()
    {
        LinkedQueue<BTNode<T>> queue = new LinkedQueue<BTNode<T>>();
        ArrayList<T> iter = new ArrayList<T>();

        if (root != null)
        {
            queue.enqueue(root);
            while (!queue.isEmpty())
            {
                BTNode<T> current = queue.dequeue();

                iter.add (current.getElement());

                if (current.getLeft() != null)
                    queue.enqueue(current.getLeft());
                if (current.getRight() != null)
                    queue.enqueue(current.getRight());
            }
        }

        return iter;
    }


    public ArrayList<T> iterator()
    {
        return inorder();
    }


    public boolean isEmpty() {
        if(root.count()==0){
            return true;
        }
        return false;
    }

    public String toString() {
//        ArrayList<T> i = new ArrayList<>();
//        i=inorder();
//        String result=i.toString();
//        return result;
        return super.toString();
    }

    public ArrayList<T> preorder() {
        ArrayList<T> iter=new ArrayList<>();

        if(root!=null){
            root.preorder(iter);
        }

        return iter;
    }
    public ArrayList<T> postorder() {
        ArrayList<T> iter=new ArrayList<>();

        if(root!=null){
            root.postorder(iter);
        }

        return iter;
    }
    public LinkedBinaryTree2<T> creat(String pre, String in) {
        String a = String.valueOf(pre.charAt(0));
        int b = in.indexOf(a);
        String preleft = pre.substring(1, b + 1);
        String preright = pre.substring(b + 1);
        String inleft = in.substring(0, b);
        String inright = in.substring(b + 1);
        LinkedBinaryTree2<T> lefttree = new LinkedBinaryTree2<T>();
        LinkedBinaryTree2<T> rightree = new LinkedBinaryTree2<T>();
        if (inleft.length() > 1) {
            lefttree = lefttree.creat(preleft, inleft);
        } else if (inleft.length() == 1)
            lefttree = new LinkedBinaryTree2<T>((T) inleft);
        if (inright.length() > 1) {
            rightree = rightree.creat(preright, inright);
        } else if (inright.length() == 1)
            rightree = new LinkedBinaryTree2<T>((T) inright);
        LinkedBinaryTree2 result = new LinkedBinaryTree2<T>((T) a, lefttree, rightree);
        return result;
    }
}