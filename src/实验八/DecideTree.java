public class DecideTree {
    private BinaryTreeADT root;
    protected String s;
    DecideTree(){
        root=new BinaryTreeADT("你喜欢学java吗？");
        root.left=new BinaryTreeADT("志强老师帅吗？");
    }

    void run(){
        System.out.println(root.element);
    }
    int run(String a) {
        switch (a) {
            case "yes": {
                root = root.left;
                break;
            }
            case "no": {
                root = root.right;
                break;
            }
            default:
                System.out.println("illegal input");
        }
        if (root != null) {
            System.out.println(root.element);
            return 1;
        }
        else
            return -1;
    }
}