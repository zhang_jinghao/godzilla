public class LinearNode<T> {
    private LinearNode<T> next;
    private T element;
    private T noce;
    LinearNode()
    {
        next = null;
        element = null;
        noce = null;
    }
    public LinearNode(T elem)
    {
        next = null;
        element = elem;
    }
    LinearNode<T> getNext()
    {
        return next;
    }
    void setNext(LinearNode<T> node)
    {
        next = node;
    }
    T getElement()
    {
        return element;
    }
    void setElement(T elem)
    {
        element = elem;
    }
    T getNoce()
    {
        return noce;
    }
    void setNoce(T ele)
    {
        noce=ele;
    }
}