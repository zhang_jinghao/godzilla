import junit.framework.TestCase;

public class LinkedBinaryTreeTest extends TestCase {

    private LinkedBinaryTree a1;
    private LinkedBinaryTree a2;
    private LinkedBinaryTree a3;
    private LinkedBinaryTree a4;
    private LinkedBinaryTree a5;

    {
        a1 = new LinkedBinaryTree(1);
        a2 = new LinkedBinaryTree(2);
        a3 = new LinkedBinaryTree(3, l1, l2);
        a4=new LinkedBinaryTree(4);
        a5=new LinkedBinaryTree(5,l3,l4);
    }


    public void testGetRootElement() {
    }

    public void testGetLeft() {
        assertEquals(1,l3.getLeft().root.element);
        assertEquals(3,l5.getLeft().root.element);
    }

    public void testGetRight() {
        assertEquals(2,l3.getRight().root.element);
        assertEquals(4,l5.getRight().root.element);
    }

    public void testContains() {
        assertTrue(l1.contains(1));
        assertFalse(l1.contains(2));
        assertTrue(l2.contains(2));
        assertFalse(l2.contains(1));
        assertTrue(l5.contains(1));
        assertTrue(l5.contains(2));
        assertTrue(l5.contains(3));
        assertTrue(l5.contains(4));
        assertTrue(l5.contains(5));

    }

    public void testFind() {
        assertEquals("[5, 3, 4, 1, 2]",l5.levelorder().toString());
        assertEquals("[1]",l1.levelorder().toString());
        assertEquals("[2]",l2.levelorder().toString());
        assertEquals("[3, 1, 2]",l3.levelorder().toString());
    }

    public void testSize() {
        assertEquals(1,l1.size());
        assertEquals(1,l2.size());
        assertEquals(3,l3.size());
        assertEquals(1,l4.size());
        assertEquals(5,l5.size());
    }

    public void testInorder() {
        assertEquals("[1]",l1.inorder().toString());
        assertEquals("[2]",l2.inorder().toString());
        assertEquals("[1, 3, 2]",l3.inorder().toString());
        assertEquals("[4]",l4.inorder().toString());
        assertEquals("[1, 3, 2, 5, 4]",l5.inorder().toString());
    }

    public void testIsEmpty() {
        assertEquals(false,l1.isEmpty());
        assertEquals(false,l2.isEmpty());
        assertEquals(false,l3.isEmpty());
        assertEquals(false,l4.isEmpty());
        assertEquals(false,l5.isEmpty());
    }


    public void testPreorder() {
        assertEquals("[1]",l1.preorder().toString());
        assertEquals("[2]",l2.preorder().toString());
        assertEquals("[3, 1, 2]",l3.preorder().toString());
        assertEquals("[4]",l4.preorder().toString());
        assertEquals("[5, 1, 3, 2, 4]",l5.preorder().toString());

    }

    public void testPostorder() {
        assertEquals("[1]",l1.postorder().toString());
        assertEquals("[2]",l2.postorder().toString());
        assertEquals("[1, 2, 3]",l3.postorder().toString());
        assertEquals("[4]",l4.postorder().toString());
        assertEquals("[1, 3, 2, 4, 5]",l5.postorder().toString());
    }
}