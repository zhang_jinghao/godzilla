class TreeNode{
    String data;
    TreeNode left ;
    TreeNode right;
    public String getData()
    {
        return data;
    }
    public TreeNode getLchild()	{
        return left;
    }
    public TreeNode getRchild()	{
        return right;
    }
    public void setNode(String data, TreeNode left2, TreeNode right2){
        this.data = data;
        left = left2;
        right = right2;
    }
    public TreeNode(){

    }
}
