import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

public class Server4 {
    public static void main(String[] args) throws IOException, ScriptException {
        //建立一个服务器ServerSocket绑定指定端口
        ServerSocket serverSocket=new ServerSocket(8800);
        //使用accept()方法阻止等待监听，获得新连接
        Socket socket=serverSocket.accept();
        //输入流
        InputStream inputStream=socket.getInputStream();
        BufferedReader bufferedReader=new BufferedReader(new InputStreamReader(inputStream));
        //输出流
        OutputStream outputStream=socket.getOutputStream();
        PrintWriter printWriter=new PrintWriter(outputStream);
        //读取用户输入信息
        String info=null;
        System.out.println("服务器已经建立......");
        info = bufferedReader.readLine();
        System.out.println("我是服务器，用户信息为：" + info);
        String reply = "";
        script s = new script(info);
        reply = s.getresult();
        printWriter.write(reply);
        printWriter.flush();

        //关闭资源
        printWriter.close();
        outputStream.close();
        bufferedReader.close();
        inputStream.close();
        socket.close();
        serverSocket.close();
    }
}