public class Sort {
    public static <T extends Comparable<T>> void selectionSort(T[] data)
    {
                int[] arr = new int[] { 5, 3, 6, 2, 10, 2, 1 };
                selectSort(arr);
                for (int i = 0; i < arr.length; i++) {
                    System.out.print(arr[i] + " ");
                }
            }

            public static void selectSort(int[] arr) {
                for (int i = 0; i < arr.length - 1; i++) {
                    int minIndex = i;
                    for (int j = i + 1; j < arr.length; j++) {
                        if (arr[j] < arr[minIndex]) {
                            minIndex = j;
                        }
                    }
                    if (i != minIndex) {
                        int temp = arr[i];
                        arr[i] = arr[minIndex];
                        arr[minIndex] = temp;
                    }
                }
            }

        }
    }

    private static <T extends Comparable<T>> void swap(T[] data, int index1, int index2)
    {
        T temp = data[index1];
        data[index1] = data[index2];
        data[index2] = temp;
    }
    public static <T extends Comparable<T>> void insertionSort(T[] data)
    {
public static int[] sort(int[] array) {
        for (int i = 1; i < array.length; i++) {
        int temp = array[i];
        int j = i - 1;
        for (; j >= 0; j--) {
        if (array[j] > temp) {
        array[j + 1] = array[j];
        } else {
        break;
        }
        }
        array[j + 1] = temp;
        System.out.println("第" + i + "次排序后:" + Arrays.toString(array));
        }
        return array;
        }

    public static <T extends Comparable<T>> void maopaoSort(T[] data)
    {
        int position, scan;
        T temp;
        long startTime=System.nanoTime();
        int comparetimes = 0;
        for (position =  data.length - 1; position >= 0; position--)
        {
            comparetimes++;
            for (scan = 0; scan <= position - 1; scan++)
            {
                if (data[scan].compareTo(data[scan+1]) > 0)
                    swap(data, scan, scan + 1);
                comparetimes++;
            }
        }

        long endTime = System.nanoTime();
    }

    private static int comparetimes = 0;
    public static <T extends Comparable<T>> void guibingSort(T[] data)
    {
        long startTime=System.nanoTime();
        mergeSort(data, 0, data.length - 1);

        long endTime = System.nanoTime();
    }
    private static <T extends Comparable<T>> void guibingSort(T[] data, int min, int max)
    {
        if (min < max)
        {
            int mid = (min + max) / 2;
            mergeSort(data, min, mid);
            mergeSort(data, mid+1, max);
            merge(data, min, mid, max);
        }
        comparetimes++;
    }
    private static <T extends Comparable<T>>void guibingSort(T[] data, int first, int mid, int last)
    {
        T[] temp = (T[])(new Comparable[data.length]);

        int first1 = first, last1 = mid;
        int first2 = mid+1, last2 = last;
        int index = first1;
        while (first1 <= last1 && first2 <= last2)
        {
            if (data[first1].compareTo(data[first2]) < 0)
            {
                temp[index] = data[first1];
                first1++;
            }
            else
            {
                temp[index] = data[first2];
                first2++;
            }
            index++;
        }

        while (first1 <= last1)
        {
            temp[index] = data[first1];
            first1++;
            index++;
        }

        while (first2 <= last2)
        {
            temp[index] = data[first2];
            first2++;
            index++;
        }

        for (index = first; index <= last; index++)
            data[index] = temp[index];
    }
    private static int time=0;
    public static <T extends Comparable<T>> void quickSort(T[] data)
    {
        long startTime=System.nanoTime();
        quickSort(data, 0, data.length - 1);

        long endTime = System.nanoTime();
    }
    private static <T extends Comparable<T>> void quickSort(T[] data, int min, int max)
    {
        time++;
        if (min < max)
        {
            int indexofpartition = partition(data, min, max);
            quickSort(data, min, indexofpartition - 1);
            quickSort(data, indexofpartition + 1, max);
        }
    }
    private static <T extends Comparable<T>> int partition(T[] data, int min, int max)
    {
        T partitionelement;
        int left, right;
        int middle = (min + max) / 2;
        partitionelement = data[middle];
        swap(data, middle, min);

        left = min;
        right = max;

        while (left < right)
        {
            while (left < right && data[left].compareTo(partitionelement) <= 0)
                left++;
            while (data[right].compareTo(partitionelement) > 0)
                right--;

            if (left < right)
                swap(data, left, right);
        }
        swap(data, min, right);

        return right;
    }
}