public class MyLink {
    Node head = null; // 设置头节点
    int n = 0;
    public void addNode(int d) {
        Node newNode = new Node(d);// 对一个结点进行实例化
        if (head == null) {
            head = newNode;
            n++;
            return;
        }
        Node temp = head;
        while (temp.getNext() != null) temp = temp.getNext();
        temp.setNext(newNode);
        n++;
    }

    public void insertNode(int index,int d){
        if(index>n||index<0){
            System.out.println("超出范围");
            return;
        }

        Node newNode = new Node(d);
        Node temp = head;
        int i;
        for(i=0;i<index-1;i++){
            temp = temp.getNext();
        }
        newNode.setNext(temp.getNext());
        temp.setNext(newNode);
        n++;
    }

    public boolean deleteNode(int index) {
        if (index < 1 || index > length()) {
            System.out.println("超出范围");
            return false;
        }
        if (index == 1) {
            head = head.getNext();
            n--;
            return true;
        }
        int i = 1;
        Node preNode = head;
        Node curNode = preNode.getNext();
        while (curNode != null) {
            if (i == index-1) {
                preNode.setNext(curNode.getNext());
                n--;
                return true;
            }
            preNode = curNode;
            curNode = curNode.getNext();
            i++;
        }
        return false;
    }

    public int length() {
        return n;
    }

    public boolean deleteNode11(Node n) {
        if (n == null || n.getNext() == null) {
            return false;
        }
        int tmp = n.data;
        n.data = n.getNext().data;
        n.getNext().data = tmp;
        n.setNext(n.getNext().getNext());
        System.out.println("删除成功！");
        return true;
    }

    public void printList() {
        Node tmp = head;
        while (tmp != null) {
            System.out.print(tmp.data+" ");
            tmp = tmp.getNext();
        }
        System.out.println();
    }

    public void bubbleSort(){
        Node newHead = head;
        for (int i = 0; i < n; i++) {
            Node preNode = null;
            // 目前的元素
            Node node = newHead;
            for (int j = 0; j < n - 1 - i; j++) {
                if (node.data > node.getNext().data) {
                    // 1、两个结点的引用交换一下，然后node的指针交换后前移一位，preNode的指向会更新
                    // 2、缓存下下个节点
                    Node tmpNode = node.getNext().getNext();
                    node.getNext().setNext(node);
                    // 3、前驱节点指向nextNode
                    // 4、更新头结点，使得当前节点为头结点，
                    if (preNode != null) preNode.setNext(node.getNext());
                    else {
                        newHead = node.getNext();
                    }
                    // 需要把preNode指向还原为最初的下一个节点，所以赋值preNode，并把preNode后移一位
                    preNode = node.getNext();
                    /* 5、node指向下下个节点 */
                    node.setNext(tmpNode);
                    // 更新指针
                } else {
                    preNode = node;
                    node = node.getNext();
                }
            }
        }
    }
}