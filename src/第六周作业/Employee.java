public class Employee extends StaffMember {
    protected String socialSecurityNumber;
    protected double payRate;

    public Employee(String eName, String eAddress, String ePhone, String socSecnumber, double rate, int eVacation)
    {
        super(eName,eAddress,ePhone,eVacation);
        socialSecurityNumber=socSecnumber;
        payRate=rate;
    }

    public String toString() {
        String result=super.toString();
        result +="\nsocial security number:"+ socialSecurityNumber;
        return result;
    }

    public double pay()
    {
        return payRate;
    }
}