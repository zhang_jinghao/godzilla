import java.util.Scanner;
public class MyLinkTest3 {
    public static void main(String[] args) {
        int []n,k;
        MyLink link = new MyLink();
        Scanner scan = new Scanner(System.in);
        for (k=0;k<9;k++){
            n=scan.nextInt();
            MyLink link;
            link.addNode(n);
        }
        for (int i = 0; i < n.length-1; i++) {
            int minIndex = i;
            for (int j = i + 1; j < n.length; j++) {
                if (n[minIndex] > n[j]) {
                    minIndex = j;
                }
            }
            if (minIndex != i) {
                int temp;
                temp = n[minIndex];
                n[minIndex] = n[i];
                n[i] = temp;
            }
        }
        link.printList();
        System.out.println("元素总数"+link.length());
    }
}
