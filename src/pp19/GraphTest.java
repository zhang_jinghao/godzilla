import java.util.Scanner;
public class GraphTest {
    public static void main(String[] args) {
        int num1=5,num2=9;
        Graph a=new Graph(num1,num2);
        a.setGraph();
        System.out.println("从节点1开始深度遍历：");
        a.DFSTraverse(1);
        System.out.println("从节点1开始广度遍历：");
        a.BFST(1);
        Scanner in=new Scanner(System.in);
        System.out.println("请输入添加的节点：");
        char x=in.nextLine().charAt(0);
        a.add(x);
        System.out.println("请输入添加的边的头部：");
        char b=in.nextLine().charAt(0);
        System.out.println("请输入添加的边的尾部：");
        char c=in.nextLine().charAt(0);
        a.add(b);
        System.out.println("请输入删除的节点：");
        char y=in.nextLine().charAt(0);
        a.delete(y);
    }
}