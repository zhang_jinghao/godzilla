public class ContactTest {
    private static Contact[] players=new Contact[5];
    public static void main(String[] args) {

        players[0] = new Contact ("zjh", "hp", "114");
        players[1] = new Contact ("pmd", "Ro", "514");
        players[2] = new Contact ("sgr", "va", "191");
        players[3] = new Contact ("wmh", "am", "226");
        players[4] = new Contact ("sjw", "hw", "918");


        Contact search1=new Contact("sjw","","");
        Contact search2=new Contact("","","114");

        //线性查找
        System.out.println("LinearSearch1:"+"\n"+LinearSearch(search1));
        System.out.println("LinearSearch2:"+"\n"+LinearSearch(search2));

        //折中查找
        System.out.println("BinarySearch1:"+"\n"+BinarySearch(search1));
        System.out.println("BinarySearch2:"+"\n"+BinarySearch(search2));
    }
    private static String LinearSearch(Contact searchElement){
        boolean searchSuccess=false;
        String result="";
        for (int i=0;i<players.length;i++){
            if (players[i].compareTo(searchElement)==0){
                result="Found!At locate ["+i+"]";
                searchSuccess=true;
                break;
            }
        }
        if (searchSuccess) {
            return result;
        }
        result="Not Found!";
        return result;
    }
    public static String BinarySearch(Contact searchElement){
        //排序
        for (int i=0;i<players.length-1;i++){
            for (int j=i+1;j<players.length;j++){
                if (players[i].compareTo(players[j])>0){
                    Contact temp=players[i];
                    players[i]=players[j];
                    players[j]=temp;
                }
            }
        }

        //查找的一系列步骤
        int high,low,middle;
        low=0;
        high=players.length-1;
        middle=(low+high)/2;
        boolean searchSuccess=false;
        String result="";

        while (!false){
            if (low>high){
                result="not found!";
                break;
            }
            else {
                if (players[middle].compareTo(searchElement)==0){
                    searchSuccess=true;
                    result="找到 ["+middle+"]";
                    break;
                }
                else if (searchElement.compareTo(players[middle])<0){
                    high=middle-1;
                }
                else if (searchElement.compareTo(players[middle])>0){
                    low=middle+1;
                }
                middle=(low+high)/2;
            }
        }
        return result;
    }
}