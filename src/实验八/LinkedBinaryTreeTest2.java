import java.util.Scanner;


public class LinkedBinaryTreeTest2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入中序序列：");
        String inOrder = scanner.nextLine();
        System.out.println("请输入先序序列：");
        String preOrder = scanner.nextLine();
        String[] in = inOrder.split("s+");
        String[] pre = preOrder.split("s+");

        LinkedBinaryTree2 tree = ReConstructBinaryTreeCore(in,pre);

        System.out.println("后序遍历结果为");
        System.out.println(tree.toString());

    }


    public static LinkedBinaryTree2 ReConstructBinaryTreeCore(String[] in, String[] pre)
    {
        LinkedBinaryTree2 tree;
        if(pre.length == 0 || in.length == 0 || pre.length != in.length){
            tree =  new LinkedBinaryTree2();
        }
        else {
            int x = 0;
            while (!(in[x] .equals( pre[0]))) {
                x++;
            }

            String[] inLeft = new String[x];
            String[] preLeft = new String[x];
            String[] inRight = new String[in.length - x - 1];
            String[] preRight = new String[pre.length - x - 1];

            for (int y = 0; y < in.length; y++) {
                if (y < x) {
                    inLeft[y] = in[y];
                    preLeft[y] = pre[y + 1];
                } else if (y > x) {
                    inRight[y - x - 1] = in[y];
                    preRight[y - x - 1] = pre[y];
                }
            }
            LinkedBinaryTree2 left = ReConstructBinaryTreeCore(inLeft, preLeft);
            LinkedBinaryTree2 right = ReConstructBinaryTreeCore(inRight, preRight);
            tree = new LinkedBinaryTree2(pre[0], left,right);
        }
        return tree;
    }
}
