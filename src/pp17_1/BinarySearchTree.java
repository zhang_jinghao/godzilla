public interface BinarySearchTree<T extends Comparable<T>> extends BinaryTree<T>
{
    public void add(T element);
    public T find(T target);
    public T findMin();
    public T findMax();
    public T remove(T target);
}