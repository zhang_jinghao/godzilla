public class Contact implements Comparable{
    private String firstName,lastName,phone;
    public Contact(String firstName, String lastName, String phone) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.phone = phone;
    }
    public String toString() {
        return "firstName:" + firstName + ";lastName:" + lastName +";phone:" + phone;
    }
    public int compareTo(Object o) {
        int result;
        result=phone.compareTo(((Contact)o).phone);
        return result;
    }
}