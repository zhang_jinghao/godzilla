import java.util.regex.Matcher;
import java.util.regex.Pattern;

class  Regex
{

    public Matcher getMatcher(String regex_type , String str )
    {

        Matcher matcher;
        if(regex_type.equals("general_regex"))
        {
            matcher=init_general_regex(str);
        }


        else if(regex_type.equals("num_regex"))
        {
            matcher=init_num_regex(str);
        }


        else if(regex_type.equals("blank_regex"))
        {
            matcher=init_blank_regex(str);
        }


        else if(regex_type.equals("left_bracket_regex"))
        {
            matcher=init_left_bracket_regex(str);
        }

        else if(regex_type.equals("right_bracket_regex"))
        {
            matcher=init_right_bracket_regex(str);
        }


        else if(regex_type.equals("add_reduce_regex"))
        {
            matcher=init_add_reduce_regex(str);
        }

        else if(regex_type.equals("multiplication_division_regex"))
        {
            matcher=init_multiplication_division_regex(str);
        }

        else
        {
            System.out.println("invalid param,object no found,Matcher will be set to general_regex");
            matcher=init_general_regex(str);
        }

        return matcher;
    }

    public Matcher init_general_regex(String s)
    {
        String general_regex="(([0-9]+\\.[0-9]+)|([0-9]+))|(\\s+)|([/(])|([/)])|([*|//])|([+|-])";
        Pattern general_regex_pattern = Pattern.compile(general_regex);
        Matcher general_regex_matcher =general_regex_pattern.matcher(s);
        return general_regex_matcher;
    }

    public Matcher init_num_regex(String s)
    {
        String num_regex="(([0-9]+\\.[0-9]+)|([0-9]+))";
        Pattern num_regex_pattern = Pattern.compile(num_regex);
        Matcher num_regex_matcher =num_regex_pattern.matcher(s);
        return num_regex_matcher;
    }

    public Matcher init_blank_regex(String s)
    {

        String blank_regex="\\s+";
        Pattern blank_regex_pattern = Pattern.compile(blank_regex);
        Matcher blank_regex_matcher =blank_regex_pattern.matcher(s);
        return blank_regex_matcher;

    }

    public Matcher init_left_bracket_regex(String s)
    {
        String left_bracket_regex="[/(]";
        Pattern left_bracket_pattern = Pattern.compile(left_bracket_regex);
        Matcher left_bracket_matcher =left_bracket_pattern.matcher(s);
        return  left_bracket_matcher;
    }

    public Matcher init_right_bracket_regex(String s)
    {

        String right_bracket_regex="[/)]";
        Pattern right_bracket_pattern = Pattern.compile(right_bracket_regex);
        Matcher right_bracket_matcher =right_bracket_pattern.matcher(s);
        return right_bracket_matcher;

    }

    public Matcher init_add_reduce_regex(String s)
    {

        String add_reduce_regex="[+|-]";
        Pattern add_reduce_pattern = Pattern.compile(add_reduce_regex);
        Matcher add_reduce_matcher =add_reduce_pattern.matcher(s);
        return  add_reduce_matcher;
    }

    public Matcher init_multiplication_division_regex(String s)
    {

        String  multiplication_division_regex="[*|//]";
        Pattern multiplication_division_pattern = Pattern.compile(multiplication_division_regex);
        Matcher multiplication_division_matcher =multiplication_division_pattern.matcher(s);
        return  multiplication_division_matcher;

    }
}