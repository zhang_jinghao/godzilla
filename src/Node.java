import java.util.*;
class Node {
    Node left;
    Node Right;
    char data;
    void print() {
        System.out.println(data + "");
    }
    public Node() {
        this.left = null;
        this.Right = null;
    }
    public Node(char data) {
        this.left = null;
        this.Right = null;
        this.data = data;
    }
}
class BTree {
    static Node root = new Node();
    static char ch[];
    static int i = 0;
    static Node CreateTree()
    {
        Node node = null;
        if (ch[i] == '#') {
            node = null;
            i++;
        }else {
            node = new Node();
            node.data = ch[i];
            i++;
            node.left = CreateTree();
            node.Right = CreateTree();
        }
        return node;
    }
    static public void preorder(Node node)
    {
        if (node != null) {
            node.print();
            preorder(node.left);
            preorder(node.Right);
        } else {
            System.out.println("");
        }
    }

    public static void main(String args[]) {
        Scanner reader = new Scanner(System.in);
        System.out.print("\n递归层序遍历 ：\n");
        BTree.ch = new char[16];
        BTree.ch[0] = 'a';
        BTree.ch = "AB#CD###E#F##".toCharArray();
        BTree.root = BTree.CreateTree();
        BTree.preorder(BTree.root);
    }
}