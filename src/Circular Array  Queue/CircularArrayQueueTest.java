public class CircularArrayQueueTest {

    public static void main(String[] args) {
        CircularArrayQueue caq = new CircularArrayQueue();

        System.out.println(caq.toString());
        System.out.println("空的？ "+caq.isEmpty());
        System.out.println(caq.toString());
        System.out.println(caq.toString());
        System.out.println("空的? "+caq.isEmpty());

        caq.dequeue(6);
        caq.dequeue(8);
        caq.dequeue(11);
        caq.dequeue(16);

        System.out.println(caq.toString());

        caq.dequeue(11);
        caq.dequeue(45);
        caq.dequeue(14);
        caq.dequeue(19);
        caq.dequeue(91);
        caq.dequeue(38);

        System.out.println(caq.toString());
        System.out.println("空的? "+caq.isEmpty());
    }
}










