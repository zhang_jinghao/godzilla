import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;

public class HuffmanMakeCode {
    public static char[] word = new char[]{'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s'
            ,'t','u','v','w','x','y','z',' '};
    public static int[] number = new int[]{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

    public static String makecode(FileInputStream stream) throws IOException {
        //读取文件
        BufferedInputStream in = new BufferedInputStream(stream);
        //决定了一次性取多少字节
        byte[] bytes = new byte[2048];
        //接受读取的内容
        int n = -1;
        String a = null;
        //进行循环操作
        while ((n = in.read(bytes, 0, bytes.length)) != -1) {
            //将n转换成字符串
            a = new String(bytes, 0, n, "GBK");
        }

        // 统计数目
        count(a);

        return a;
    }

    public static void count(String str){
        for (int i = 0;i < 27;i++){
            int num = 0;
            for (int j = 0;j < str.length();j++){
                if (str.charAt(j) == word[i]){
                    num++;
                }
            }
            number[i] += num;
        }
    }

    public static char[] getWord() {
        return word;
    }

    public static int[] getNumber() {
        return number;
    }
}
