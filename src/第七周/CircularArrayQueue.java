public class CircularArrayQueue<T> implements QueueADT<T>
{
    private final int A = 100;
    private int a, b, c;
    private T[] queue;

    public CircularArrayQueue()//将指定元素添加到栈的顶部
    {
        a =b = c = 0;
        queue = (T[]) (new Object[A]);
    }

    public CircularArrayQueue (int initialCapacity)
    {
        a = b = c = 0;
        queue = (T[]) new Object[initialCapacity];
    }

    public void enqueue (T element)
    {
        if (size() == queue.length)
            expandCapacity();

        queue[b] = element;
        b = (b+1) % queue.length;

        c++;
    }

    public T dequeue() throws EmptyCollection
    {
        if (isEmpty())
            throw new EmptyCollection("queue");

        T result = queue[a];
        queue[a] = null;
        a = (a+1) % queue.length;

        c--;

        return result;
    }

    public T first() throws EmptyCollection
    {
        if (isEmpty())
            throw new EmptyCollection("queue");

        return queue[a];
    }

    public boolean isEmpty()//定义isEmpty操作
    {
        return (c == 0);
    }

    public int size()//定义size操作
    {
        return c;
    }

    public String toString()//定义toString操作
    {
        StringBuilder result = new StringBuilder();
        int scan = 0;

        while(scan <c)
        {
            if(queue[scan]!=null)
            {
                result.append(queue[scan].toString()).append("\n");
            }
            scan++;
        }

        return result.toString();
    }

    private void expandCapacity()
    {
        T[] larger = (T[]) new Object[queue.length *2];

        for(int scan=0; scan < c; scan++)
        {
            larger[scan] = queue[a];
            a=(a+1) % queue.length;
        }

        a= b = c=0;
        queue = larger;
    }
}