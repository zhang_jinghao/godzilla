public class CircularArrayQueue<T> {

    private int front, rear , count;
    private T[] queue;
    public CircularArrayQueue(int initialcapcity) {
        front = rear = count = 0;
        queue = (T[]) (new Object[initialcapcity]);
    }

    public void inqueue(T val) {
        if(size() == queue.length){
            expandQueue();
        }
        queue[rear] = val;
        rear = (rear+1)%queue.length;
        count++;
    }

    public T dequeue() throws Exception{
        if(size() == 0) {
            throw new Exception("queue");
        }
        T val = queue[front];
        queue[front] = null;
        front = (front+1)%queue.length;
        count--;
        return val;
    }

    public int size() {
        return count;
    }

    public void expandQueue(){
        T[] larger = (T[])(new Object[queue.length*2]);

        for(int scan = 0; scan < count; scan++){
            larger[scan] = queue[front];
            front = (front+1)%queue.length;
        }
        front = 0;
        rear = count;
        queue = larger;
    }

    public static void main(String[] args) {
        CircularArrayQueue p = new CircularArrayQueue(5);
        for(int i=0; i<5; i++){
            p.inqueue(i);
        }
        try {
            System.out.println(p.dequeue());
            System.out.println(p.dequeue());
            System.out.println(p.dequeue());
            System.out.println(p.dequeue());
            System.out.println(p.dequeue());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


