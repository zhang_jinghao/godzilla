import java.util.Random;

public class PassWord implements Encryptable {
    private String message;
    private boolean encry;
    private int shift;
    private Random gene;
    public PassWord(String msg)
    {
        message=msg;
        encry=false;
        gene=new Random();
        shift=gene.nextInt(10)+5;
    }


    public void encrypt() {
        if (!encry)
        {
            String masked="";
            for (int index=0;index<message.length();index++)
                masked=masked+(char)(shift-message.charAt (index));
            message=masked;
            encry=true;
        }
    }

    public String decrypt() {
        if (encry)
        {
            String unmasked="";
            for (int index=0;index<message.length ();index++)
                unmasked=unmasked+(char)(shift-message.charAt (index));
            message=unmasked;
            encry=false;
        }
        return message;
    }

    public boolean isEncrypted()
    {
        return encry;
    }

    public String toString() {
        return message;
    }
}

