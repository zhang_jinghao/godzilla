public class Searching {
    public static Comparable Search1(Comparable[] num,Comparable target){
        String result = null;
        int cuont = 0;

        while(result == null && cuont < num.length){
            if(num[cuont].compareTo(target) == 0)
                result = num[cuont];
            cuont++;
        }
        return result;
    }
    public static Comparable Search2(Comparable[] num,Comparable target){
        Comparable result = null;
        int first = 0,last = num.length-1,mid;
        while (result == null && first <= last){
            mid = (first + last) / 2;
            if(num[mid].compareTo(target) == 0)
                result = num[mid];
            else
            if(num[mid].compareTo(target) > 0)
                last = mid - 1;
            else
                first = mid - 1;
        }
        return result;
    }
    public static Comparable Search3(Comparable[] num,int first,int last,Comparable target){
        Comparable result = null;
        int mid = (first + last) / 2;
        if(num[mid].compareTo(target) == 0)
            result = num[mid];
        else
        if(num[mid].compareTo(target)>0)
            result = Search3(num,first,mid,target);
        else
            result = Search3(num,mid,last,target);

        return result;
    }
}