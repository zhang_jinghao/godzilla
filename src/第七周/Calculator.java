import java.util.ArrayList;
import java.util.List;
import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;



class Calculator {
    //计算中缀表达式
    static Stack<String> op = new Stack<>();

    public static Float getv(String op, Float f1, Float f2){
        if(op.equals("+")) return f2 + f1;
        else if(op.equals("-")) return f2 - f1;
        else if(op.equals("*")) return f2 * f1;
        else if(op.equals("/")) return f2 / f1;
        else return Float.valueOf(-0);
    }

    public static String calrp(List<String> rp){
        Stack<Float> v = new Stack<>();

        //har[] arr = rp.toCharArray();
        //int len = arr.length;
        for(int i = 0; i < rp.size(); i++){

           ar ch=arr[i];
            //if(ch >= '0' && ch <= '9')
            Regex regex =new Regex();


            if( regex.getMatcher("num_regex",rp.get(i)).find()) v.push(Float.valueOf(rp.get(i)));

            else v.push(getv(rp.get(i), Float.valueOf(v.pop()), Float.valueOf(v.pop())));
        }
        return v.pop().toString();
    }

    public static List<String> getrp(String s){

        List<String> rp=new ArrayList<>();
        int index=0;
        Regex regex = new Regex();
        Matcher gen_matcher;
        gen_matcher=regex.getMatcher("general_regex",s);
        Stack<String> op = new Stack<>();


        while(gen_matcher.find()){

            if(regex.getMatcher("blank_regex",gen_matcher.group()).find()) continue;

                // char []ch=str.toCharArray();//复制版一份用于比较范围
                //if(ch[0] >= '0' && ch[0] <= '9')
                //if((str.equals("0"))||(str.equals("1"))||(str.equals("2"))||(str.equals("3"))||(str.equals("4"))||(str.equals("5"))||(str.equals("6"))||(str.equals("7"))||(str.equals("8"))||(str.equals("9")))
            else if(regex.getMatcher("num_regex",gen_matcher.group()).find()) {

                String num= gen_matcher.group();

                rp.add(index,num);
                index++;
                continue;
            }

            else if(regex.getMatcher("left_bracket_regex",gen_matcher.group()).find())

            {
                String str=gen_matcher.group();
                op.push(str);
            }

            else if(regex.getMatcher("add_reduce_regex",gen_matcher.group()).find()){
                String str=gen_matcher.group();
                while(!op.empty() && (!op.peek().equals("(")))//左括号必须遇到右括号才能出栈
                {

                    rp.add(index,op.pop());
                    index++;
                }//没有比加号和减号更低的优先级所以全部出栈
                op.push(str);
                continue;
            }

            if(regex.getMatcher("multiplication_division_regex",gen_matcher.group()).find()){
                String str=gen_matcher.group();
                while(!op.empty() && (op.peek().equals("*") || op.peek().equals("/")))
                {

                    rp.add(index,op.pop());

                    index++;
                }//栈顶是加减号是才不出栈
                op.push(str);
                continue;
            }

            if(regex.getMatcher("right_bracket_regex",gen_matcher.group()).find()){
                String str=gen_matcher.group();
                while(!op.empty() && !op.peek().equals("("))
                {
                    rp.add(index,op.pop());
                    index++;
                }
                op.pop();//左括号也弹出来
                continue;
            }
        }
        while(!op.empty())
        {
            rp.add(index,op.pop());
            index++;
        }//遍历完后再把栈内剩下的其他符号输出
        return rp;
    }
}
