import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class HuffmanTest {
    public static void main(String[] args) throws IOException {
        FileInputStream stream = new FileInputStream("C:\\Users\\hp\\IdeaProjects\\untitled38\\src\\readfile.txt");
        String a = HuffmanMakeCode.makecode(stream);
        System.out.println("读取文件： " + a);
        System.out.println("次数总计");
        char[] chars = HuffmanMakeCode.getWord();
        int[] num = HuffmanMakeCode.getNumber();
        for (int i = 0;i < num.length-1;i++){
            System.out.println(chars[i] + "出现的次数： " + num[i] + "次");
        }
        System.out.println("编码结果");
        List<HuffmanNode<String>> list = new ArrayList<>();

        for (int i = 0;i < num.length-1;i++){
            boolean add = list.add(new HuffmanNode(num[i], chars[i]));
        }
        Collections.sort(list);
        HuffmanNode root = HuffmanTree.createTree(list);

        List<HuffmanNode> list1;
        list1 = HuffmanTree.BFS(root);
        List<String> list2 = new ArrayList<>();
        List<String> list3 = new ArrayList<>();
        for (HuffmanNode huffmanNode : list1) {
            if (huffmanNode.getWord() != null) {
                list2.add(String.valueOf(huffmanNode.getWord()));
                list3.add(huffmanNode.getCode());
            }
        }
        Collections.sort(list2);
        for (int i = 0;i < list3.size();i++){
            System.out.println(list2.get(i) + "的编码为： " + list3.get(i));
        }
        StringBuilder result = new StringBuilder();
        for (int i = 0;i < a.length();i++){
            for (int j = 0; j < list2.size();j++){
                if (a.charAt(i) == list2.get(j).charAt(0)){
                    result.append(list3.get(j));
                }
            }
        }
        System.out.println("文件编码结果： " + result);
        List<String> list4 = new ArrayList<>();
        for (int i = 0;i < result.length();i++){
            list4.add(result.charAt(i) + "");
        }
        StringBuilder temp = new StringBuilder();
        StringBuilder temp1 = new StringBuilder();
        while (list4.size() > 0){
            temp.append("").append(list4.get(0));
            list4.remove(0);
            for (int i = 0;i < list3.size();i++){
                if (temp.toString().equals(list3.get(i))){
                    temp1.append("").append(list2.get(i));
                    temp = new StringBuilder();
                }
            }
        }
        System.out.println("文件解码结果： " + temp1);
        File file = new File("C:\\Users\\hp\\IdeaProjects\\untitled38\\src\\outfile.txt");
        Writer out = new FileWriter(file);
        out.write(result.toString());
        out.close();
    }
}







