public class BinaryTree<T> {
    private int a;
    private int b= 0b0;
    private String str="";
    private BinaryTreeNode i2,i1;
    BinaryTreeNode root;
    private int count;
    BinaryTree(){
        root=null;
    }
    void creat(int[] a){
        count=a.length;
        for(int b=0;b<count;b++){
            BinaryTreeNode temp=  new BinaryTreeNode(a[b]);

            if(root==null) root=temp;

            else{
                BinaryTreeNode current=root;
                while(true) {
                    if(a[b]>current.Element){
                        if(current.right==null) {current.right=temp;break;}
                        else {
                            current=current.right;
                        }
                    }
                    else if(a[b]<current.Element){
                        if(current.left==null){current.left=temp;break;}
                        else{
                            current=current.left;
                        }
                    }
                }
            }
        }
    }

    boolean find(int target){

        BinaryTreeNode current=root;
        while(current!=null){
            if(current.Element==target) {return true;}
            if(current.Element<target){ current=current.right;}
            else {current=current.left;}
        }
        return false;
    }
    void del(int target){

        count--;
        int i3=0;
        BinaryTreeNode current=root;
        while(current!=null){
            if(current.Element==target) {break;}
            if(current.Element<target){
                i3=1;
                i2=current;
                current=current.right;}
            else {
                i3=2;
                i1=current;
                current=current.left;}
        }
        int y=0;
        assert current != null;
        if(current.left==null&&current.right==null){

            if(i3==1)
                i2.right=null;
            else if (i3==2)
                i1.left=null;
        }
        else if(current.left != null && current.right == null || current.left == null){
            if(current.left != null) {
                current.setElement(current.left.getElement());
                current.right=current.left.right;
                current.left=current.left.left;}
            else if(current.left == null){

                current.setElement(current.right.getElement());
                current.right=current.right.right;
                current.left=current.right.left;
            }
        }
        else  if(current.left != null){
            String r1="";
            r1=dizhong(root);
            String[] r2=r1.split(" ");

            int[] rr=new int[count+1];
            for(int y1=0;y1<=count;y1++){
                rr[y1]= Integer.parseInt(r2[y1]);
            }
            for(int r=0;r<count;r++){
                if(rr[r]==target) {
                    a=r;
                    break;
                }
            }
            BinaryTreeNode current2=root;
            while(current2!=null){
                if(current2.Element==rr[a-1]) {break;}
                if(current2.Element<rr[a-1]){
                    i3=1;
                    i2=current2;
                    current2=current2.right;}
                else if(current2.Element>rr[a-1] ){
                    i3=2;
                    i1=current2;
                    current2=current2.left;}
            }
            if(i3==1) i2.right=null;
            else if(i3==2) i1.left=null;
            current.setElement(rr[a-1]);
        }
    }
    private String dizhong(BinaryTreeNode temp){
        if(temp==null) return null;
        dizhong(temp.left);
        str+=""+temp.Element+" ";
        b++;
        dizhong(temp.right);
        return str;
    }
    void toString(BinaryTreeNode temp){
        if(temp==null) return;
        toString(temp.left);
        System.out.print(temp.Element+" ");
        toString(temp.right);
    }
    void insert(int m){
        BinaryTreeNode temp=new BinaryTreeNode(m);
        if(root==null) root=temp;

        else{
            BinaryTreeNode current=root;
            while(true) {
                if(m>current.Element){
                    if(current.right==null) {current.right=temp;break;}
                    else {
                        current=current.right;
                    }
                }
                else if(m<current.Element){
                    if(current.left==null){current.left=temp;break;}
                    else{
                        current=current.left;
                    }
                }
            }
        }
        count++;
    }
}