public class BinaryTreeNode {
    BinaryTreeNode left;
    BinaryTreeNode right;
    int Element;

     BinaryTreeNode(int element){
        left=null;
        right=null;
        Element=element;
    }

     void setElement(int element){
        Element=element;
    }

     int getElement(){
        return  Element;
    }
}