import java.util.Iterator;
import java.util.Scanner;
import java.util.Stack;

public class PostfixTest {

    public static void main(String[] args) throws EmptyCollectionException {
        String infix, again;
        int result;
        Scanner in = new Scanner(System.in);
        public static String  toSuffix(String infix) {
            String result = "";
            String[] array = infix.split("\\s+");
            Stack<LinkedBinaryTree> num = new Stack();
            Stack<LinkedBinaryTree> op = new Stack();

            for (int a = 0; a < array.length; a++) {
                if (array[a].equals("+") || array[a].equals("-") || array[a].equals("*") || array[a].equals("/")) {
                    if (op.empty()) {
                        op.push(new LinkedBinaryTree<>(array[a]));
                    } else {
                        if ((op.peek().root.element).equals("+") || (op.peek().root.element).equals("-") && array[a].equals("*") || array[a].equals("/")) {
                            op.push(new LinkedBinaryTree(array[a]));
                        } else {
                            LinkedBinaryTree right = num.pop();
                            LinkedBinaryTree left = num.pop();
                            LinkedBinaryTree temp = new LinkedBinaryTree(op.pop().root.element, left, right);
                            num.push(temp);
                            op.push(new LinkedBinaryTree(array[a]));
                        }
                    }

                } else {
                    num.push(new LinkedBinaryTree<>(array[a]));
                }
            }
            while (!op.empty()) {
                LinkedBinaryTree right = num.pop();
                LinkedBinaryTree left = num.pop();
                LinkedBinaryTree temp = new LinkedBinaryTree(op.pop().root.element, left, right);
                num.push(temp);
            }
            Iterator itr=num.pop().iteratorPostOrder();
            while (itr.hasNext()){
                result+=itr.next()+" ";
            }
            return result;
        }
        public static void main(String[] args){
            System.out.println("输入中缀表达式：");
            Scanner scan=new Scanner(System.in);
            String exp=scan.nextLine();
            System.out.println("后缀表达式为：");
            System.out.println(getrp(exp));
            System.out.println("计算结果：");
            System.out.println(calrp(getrp(exp)));

        }

    }
