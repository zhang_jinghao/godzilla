package HttpSocket;

import java.io.*;
import java.net.Socket;
public class Client {
    public static void main(String[] args) throws IOException {
        //建立客户端Socket连接，指定服务器位置和端口
        Socket socket = new Socket("192.168.1.106",8800);
//        Socket socket = new Socket("172.16.43.187",8800);

        //得到socket读写流
        OutputStream outputStream = socket.getOutputStream();
        //       PrintWriter printWriter = new PrintWriter(outputStream);
        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream);
        //输入流
        InputStream inputStream = socket.getInputStream();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream,"UTF-8"));
        //利用流按照一定的操作，对socket进行读写操作
        String info1 = " 用户名：pmd，密码：123456";
        outputStreamWriter.write(info1);
        outputStreamWriter.flush();
        socket.shutdownOutput();
        //接收服务器的响应
        String reply = null;
        while (!((reply = bufferedReader.readLine()) ==null)){
            System.out.println("接收服务器的信息为：" + reply);
        }
        //关闭资源
        bufferedReader.close();
        inputStream.close();
        outputStreamWriter.close();
        outputStream.close();
        socket.close();
    }
}