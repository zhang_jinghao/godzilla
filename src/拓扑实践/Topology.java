public class Topology {
    public static void main(String[] args) {
        AdjTag s = new AdjTag(5);
        s.insert(1, 5, 0);
        s.insert(3, 7, 0);
        s.insert(2, 4, 1);
        s.insert(0, 8, 2);
        s.insert(4, 2, 2);
        s.insert(2, 6, 3);
        s.insert(3, 3, 4);
        int[] a = new int[5];
        for (int i = 0; i < a.length; i++) {
            a[i] = s.du(i);
        }
        for (int i = 0; i < a.length; i++) {
            System.out.print("V"+i + "节点出度:");
            System.out.println(a[i]);
        }
        System.out.println("-------------------------");
        for (int i = 0; i < a.length; i++) {
            System.out.print("V"+i + "节点邻接：");
            System.out.println(s.tostring(i));
        }
    }
}